package model.game.ourcause;

import java.util.List;

public class Scene {
    private Long pkey;
    private String sceneCode; //场景代码
    private String sceneTitle; //场景名称
    private String mapCode; //地点代码
    private String previousScene; //前一个场景
    private String nextScene; //后一个场景
    private String skipSceneCode;   //跳转场景代码  
    private List<Script> focusScriptLst; //正在访问的脚本内容
    private List<Script> blurScriptrLst;  //已经访问的脚本内容
    private String tips; //场景提示
    private String historyContent;  //场景简述
    private String backgroupSoundUrl;
    private String backgroupImageUrl;    
    private Long backgroupImageX;
    private Long backgroupImageY;
    private Long backgroupImageWidth;
    private Long backgroupImageHeight; 
    private boolean isFocus; //此场景是否已访问的标识 
    private Question question; //场景问题    
    
	public Scene() {
        super();
    }
    
	public Scene(Long pkey, String sceneCode, String sceneTitle,
			String mapCode, String previousScene, String nextScene,
			String skipSceneCode, List<Script> focusScriptLst,
			List<Script> blurScriptrLst, String tips, String historyContent,
			String backgroupSoundUrl, String backgroupImageUrl,
			Long backgroupImageX, Long backgroupImageY,
			Long backgroupImageWidth, Long backgroupImageHeight,
			boolean isFocus, Question question) {
		super();
		this.pkey = pkey;
		this.sceneCode = sceneCode;
		this.sceneTitle = sceneTitle;
		this.mapCode = mapCode;
		this.previousScene = previousScene;
		this.nextScene = nextScene;
		this.skipSceneCode = skipSceneCode;
		this.focusScriptLst = focusScriptLst;
		this.blurScriptrLst = blurScriptrLst;
		this.tips = tips;
		this.historyContent = historyContent;
		this.backgroupSoundUrl = backgroupSoundUrl;
		this.backgroupImageUrl = backgroupImageUrl;
		this.backgroupImageX = backgroupImageX;
		this.backgroupImageY = backgroupImageY;
		this.backgroupImageWidth = backgroupImageWidth;
		this.backgroupImageHeight = backgroupImageHeight;
		this.isFocus = isFocus;
		this.question = question;
	}


	public Scene(Long pkey, String sceneCode, String sceneTitle,
			String mapCode, String previousScene, String nextScene,
			String skipSceneCode, List<Script> focusScriptLst,
			List<Script> blurScriptrLst, String tips, String historyContent,
			String backgroupSoundUrl, String backgroupImageUrl,
			Long backgroupImageX, Long backgroupImageY,
			Long backgroupImageWidth, Long backgroupImageHeight, boolean isFocus) {
		super();
		this.pkey = pkey;
		this.sceneCode = sceneCode;
		this.sceneTitle = sceneTitle;
		this.mapCode = mapCode;
		this.previousScene = previousScene;
		this.nextScene = nextScene;
		this.skipSceneCode = skipSceneCode;
		this.focusScriptLst = focusScriptLst;
		this.blurScriptrLst = blurScriptrLst;
		this.tips = tips;
		this.historyContent = historyContent;
		this.backgroupSoundUrl = backgroupSoundUrl;
		this.backgroupImageUrl = backgroupImageUrl;
		this.backgroupImageX = backgroupImageX;
		this.backgroupImageY = backgroupImageY;
		this.backgroupImageWidth = backgroupImageWidth;
		this.backgroupImageHeight = backgroupImageHeight;
		this.isFocus = isFocus;
	}


	public Long getPkey() {
		return pkey;
	}


	public void setPkey(Long pkey) {
		this.pkey = pkey;
	}


	public String getSceneCode() {
		return sceneCode;
	}


	public void setSceneCode(String sceneCode) {
		this.sceneCode = sceneCode;
	}


	public String getSceneTitle() {
		return sceneTitle;
	}


	public void setSceneTitle(String sceneTitle) {
		this.sceneTitle = sceneTitle;
	}


	public String getMapCode() {
		return mapCode;
	}


	public void setMapCode(String mapCode) {
		this.mapCode = mapCode;
	}


	public String getPreviousScene() {
		return previousScene;
	}


	public void setPreviousScene(String previousScene) {
		this.previousScene = previousScene;
	}


	public String getNextScene() {
		return nextScene;
	}


	public void setNextScene(String nextScene) {
		this.nextScene = nextScene;
	}


	public String getSkipSceneCode() {
		return skipSceneCode;
	}


	public void setSkipSceneCode(String skipSceneCode) {
		this.skipSceneCode = skipSceneCode;
	}


	public List<Script> getFocusScriptLst() {
		return focusScriptLst;
	}


	public void setFocusScriptLst(List<Script> focusScriptLst) {
		this.focusScriptLst = focusScriptLst;
	}


	public List<Script> getBlurScriptrLst() {
		return blurScriptrLst;
	}


	public void setBlurScriptrLst(List<Script> blurScriptrLst) {
		this.blurScriptrLst = blurScriptrLst;
	}


	public String getTips() {
		return tips;
	}


	public void setTips(String tips) {
		this.tips = tips;
	}


	public String getHistoryContent() {
		return historyContent;
	}


	public void setHistoryContent(String historyContent) {
		this.historyContent = historyContent;
	}


	public String getBackgroupSoundUrl() {
		return backgroupSoundUrl;
	}


	public void setBackgroupSoundUrl(String backgroupSoundUrl) {
		this.backgroupSoundUrl = backgroupSoundUrl;
	}


	public String getBackgroupImageUrl() {
		return backgroupImageUrl;
	}


	public void setBackgroupImageUrl(String backgroupImageUrl) {
		this.backgroupImageUrl = backgroupImageUrl;
	}


	public Long getBackgroupImageX() {
		return backgroupImageX;
	}


	public void setBackgroupImageX(Long backgroupImageX) {
		this.backgroupImageX = backgroupImageX;
	}


	public Long getBackgroupImageY() {
		return backgroupImageY;
	}


	public void setBackgroupImageY(Long backgroupImageY) {
		this.backgroupImageY = backgroupImageY;
	}


	public Long getBackgroupImageWidth() {
		return backgroupImageWidth;
	}


	public void setBackgroupImageWidth(Long backgroupImageWidth) {
		this.backgroupImageWidth = backgroupImageWidth;
	}


	public Long getBackgroupImageHeight() {
		return backgroupImageHeight;
	}


	public void setBackgroupImageHeight(Long backgroupImageHeight) {
		this.backgroupImageHeight = backgroupImageHeight;
	}


	public boolean isFocus() {
		return isFocus;
	}


	public void setFocus(boolean isFocus) {
		this.isFocus = isFocus;
	}
	
    public Question getQuestion() {
		return question;
	}


	public void setQuestion(Question question) {
		this.question = question;
	}
	
}
