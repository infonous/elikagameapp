package model.game.ourcause;

public class MapInfo {

	public String mapCode;
	public String mapTitle;
	
	public MapInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MapInfo(String mapCode, String mapTitle) {
		super();
		this.mapCode = mapCode;
		this.mapTitle = mapTitle;
	}

	public String getMapCode() {
		return mapCode;
	}

	public void setMapCode(String mapCode) {
		this.mapCode = mapCode;
	}

	public String getMapTitle() {
		return mapTitle;
	}

	public void setMapTitle(String mapTitle) {
		this.mapTitle = mapTitle;
	}
	
	
}
