/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.game.ourcause;

public class Question {
    private Long pkey;
    private String questionCode;
    private String questionContent;
    private String compare;
    private String values;    
    private String backgroupSoundUrl;
    private String backgroupImageUrl;
    private Long backgroupImageX;
    private Long backgroupImageY;
    private Long backgroupImageWidth;
    private Long backgroupImageHeight;

    public Question(Long pkey, String questionCode, String questionContent, String compare, String values, String backgroupSoundUrl, String backgroupImageUrl, Long backgroupImageX, Long backgroupImageY, Long backgroupImageWidth, Long backgroupImageHeight) {
        this.questionCode = questionCode;
        this.questionContent = questionContent;
        this.compare = compare;
        this.values = values;
        this.backgroupSoundUrl = backgroupSoundUrl;
        this.backgroupImageUrl = backgroupImageUrl;
        this.backgroupImageX = backgroupImageX;
        this.backgroupImageY = backgroupImageY;
        this.backgroupImageWidth = backgroupImageWidth;
        this.backgroupImageHeight = backgroupImageHeight;
    }
    
    public Long getPkey() {
        return pkey;
    }

    public void setPkey(Long pkey) {
        this.pkey = pkey;
    }

    public String getQuestionCode() {
        return questionCode;
    }

    public void setQuestionCode(String questionCode) {
        this.questionCode = questionCode;
    }

    public String getQuestionContent() {
        return questionContent;
    }

    public void setQuestionContent(String questionContent) {
        this.questionContent = questionContent;
    }

    public String getCompare() {
        return compare;
    }

    public void setCompare(String compare) {
        this.compare = compare;
    }

    public String getValues() {
        return values;
    }

    public void setValues(String values) {
        this.values = values;
    }

    public String getBackgroupSoundUrl() {
        return backgroupSoundUrl;
    }

    public void setBackgroupSoundUrl(String backgroupSoundUrl) {
        this.backgroupSoundUrl = backgroupSoundUrl;
    }

    public String getBackgroupImageUrl() {
        return backgroupImageUrl;
    }

    public void setBackgroupImageUrl(String backgroupImageUrl) {
        this.backgroupImageUrl = backgroupImageUrl;
    }

    public Long getBackgroupImageX() {
        return backgroupImageX;
    }

    public void setBackgroupImageX(Long backgroupImageX) {
        this.backgroupImageX = backgroupImageX;
    }

    public Long getBackgroupImageY() {
        return backgroupImageY;
    }

    public void setBackgroupImageY(Long backgroupImageY) {
        this.backgroupImageY = backgroupImageY;
    }

    public Long getBackgroupImageWidth() {
        return backgroupImageWidth;
    }

    public void setBackgroupImageWidth(Long backgroupImageWidth) {
        this.backgroupImageWidth = backgroupImageWidth;
    }

    public Long getBackgroupImageHeight() {
        return backgroupImageHeight;
    }

    public void setBackgroupImageHeight(Long backgroupImageHeight) {
        this.backgroupImageHeight = backgroupImageHeight;
    }
}
