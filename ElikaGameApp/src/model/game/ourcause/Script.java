package model.game.ourcause;

public class Script {
    private Long pkey;
    private String sceneCode; //所属的场景代码
    private Long scriptSort; //脚本序号
    private String speakerCode; //对话者代码
    private String content; //内容
    private Question question; //问题
    private String backgroupSoundUrl;
    private String backgroupImageUrl;    
    private Long backgroupImageX;
    private Long backgroupImageY;
    private Long backgroupImageWidth;
    private Long backgroupImageHeight;    

    public Script() {
        super();
    }

    public Script(Long pkey, String sceneCode, Long scriptSort, String speakerCode, String content, Question question, String backgroupSoundUrl, String backgroupImageUrl, Long backgroupImageX, Long backgroupImageY, Long backgroupImageWidth, Long backgroupImageHeight) {
        this.pkey = pkey;
        this.sceneCode = sceneCode;
        this.scriptSort = scriptSort;
        this.speakerCode = speakerCode;
        this.content = content;
        this.question = question;
        this.backgroupSoundUrl = backgroupSoundUrl;
        this.backgroupImageUrl = backgroupImageUrl;
        this.backgroupImageX = backgroupImageX;
        this.backgroupImageY = backgroupImageY;
        this.backgroupImageWidth = backgroupImageWidth;
        this.backgroupImageHeight = backgroupImageHeight;
    }

    public Long getPkey() {
        return pkey;
    }

    public void setPkey(Long pkey) {
        this.pkey = pkey;
    }

    public String getSceneCode() {
        return sceneCode;
    }

    public void setSceneCode(String sceneCode) {
        this.sceneCode = sceneCode;
    }

    public Long getScriptSort() {
        return scriptSort;
    }

    public void setScriptSort(Long scriptSort) {
        this.scriptSort = scriptSort;
    }

    public String getSpeakerCode() {
        return speakerCode;
    }

    public void setSpeakerCode(String speakerCode) {
        this.speakerCode = speakerCode;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public String getBackgroupSoundUrl() {
        return backgroupSoundUrl;
    }

    public void setBackgroupSoundUrl(String backgroupSoundUrl) {
        this.backgroupSoundUrl = backgroupSoundUrl;
    }

    public String getBackgroupImageUrl() {
        return backgroupImageUrl;
    }

    public void setBackgroupImageUrl(String backgroupImageUrl) {
        this.backgroupImageUrl = backgroupImageUrl;
    }

    public Long getBackgroupImageX() {
        return backgroupImageX;
    }

    public void setBackgroupImageX(Long backgroupImageX) {
        this.backgroupImageX = backgroupImageX;
    }

    public Long getBackgroupImageY() {
        return backgroupImageY;
    }

    public void setBackgroupImageY(Long backgroupImageY) {
        this.backgroupImageY = backgroupImageY;
    }

    public Long getBackgroupImageWidth() {
        return backgroupImageWidth;
    }

    public void setBackgroupImageWidth(Long backgroupImageWidth) {
        this.backgroupImageWidth = backgroupImageWidth;
    }

    public Long getBackgroupImageHeight() {
        return backgroupImageHeight;
    }

    public void setBackgroupImageHeight(Long backgroupImageHeight) {
        this.backgroupImageHeight = backgroupImageHeight;
    }    
}
