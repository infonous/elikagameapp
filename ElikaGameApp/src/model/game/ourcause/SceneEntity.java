/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.game.ourcause;

import java.util.ArrayList;
import java.util.List;

public class SceneEntity {
	
	private List<Scene> sceneStore = new ArrayList<Scene>();
	private String history;
	private String currentSence;
	private String meno = "";
	private int scriptIndex = 0;
    
    public String getMeno() {
		return meno;
	}

	public void setMeno(String meno) {
		this.meno = meno;
	}

	public int getScriptIndex() {
		return scriptIndex;
	}

	public void setScriptIndex(int scriptIndex) {
		this.scriptIndex = scriptIndex;
	}

	public List<Scene> getSceneStore() {
		return sceneStore;
	}

	public void setSceneStore(List<Scene> sceneStore) {
		this.sceneStore = sceneStore;
	}

	public String getHistory() {
		return history;
	}

	public void setHistory(String history) {
		this.history = history;
	}

	public String getCurrentSence() {
		return currentSence;
	}

	public void setCurrentSence(String currentSence) {
		this.currentSence = currentSence;
	}

	public SceneEntity(){     	
        super();
    }
    	
    public SceneEntity(List<Scene> sceneStore, String history,
			String currentSence) {
		super();
		this.sceneStore = sceneStore;
		this.history = history;
		this.currentSence = currentSence;
	}

    //查找地点的最后一次场景
	public Scene getCurrentScene(String mapCode){  
    	
		Scene currentScene = null;		
		String previousScene = ""; //脚本中当前场景的前一个场景的编号，用于条件判断	 //previousSceneFlg			
		
        for(int i=0; i < sceneStore.size(); i++ ){
        	
            Scene sceneItem = (Scene)(sceneStore.get(i));            
            if( mapCode.equals(sceneItem.getMapCode()) ){ //同一个地点
            	
            	currentSence = sceneItem.getSceneCode();
            	previousScene = (sceneItem.getPreviousScene()==null? currentSence : sceneItem.getPreviousScene());              
                
                if( (history.indexOf(currentSence) == -1) ){ //此场景的第一次访问
                    
                	//如果符合条件：已访问过前一个场景
                	if( (history.indexOf(previousScene) > -1) || (previousScene.toString() == currentSence.toString()) ){ //是否满足访问的必要条件              		
                		
                		//若无须回答问题的话，则直接加入历史浏览
                		if(sceneItem.getQuestion()==null){
                			history += (history.length() > 0 ? ";" : "") + currentSence; //如已访问过场景的，加入 ; 作分隔符
                			meno = sceneItem.getHistoryContent(); //如已访问过场景的，加入 ; 作分隔符   
                		}
                		
                        sceneItem.setFocus(true);
                        currentScene = sceneItem;
                    }else{
                    	//不符合条件：已访问过前一个场景                    	
                        sceneItem.setFocus(false);
                        currentScene = sceneItem;
                    }
                	
                	break;        
                	
                }else if( (history.indexOf(currentSence) > -1) ){ //此地点的第 N 次访问，且此场景已经访问过
                	
                	//覆盖性取最后一次场景
                    sceneItem.setFocus(false);
                    currentScene = sceneItem;
                }
            } 
        }
        
        return currentScene;
    }
	
	//场景直接跳转，可以直接指定从剧本的第  scriptIndex 段开始
	public Scene getSkipToScene(String scenceCode, Long skipScriptIndex){
		
		Scene currentScene = null;		
		for(int i=0; i < sceneStore.size(); i++ ){	
			
			Scene sceneItem = (Scene)(sceneStore.get(i)); 
			if(scenceCode.equals(sceneItem.getSceneCode())){
				
				currentSence = sceneItem.getSceneCode();
				if ((sceneItem.getQuestion()==null) && (history.indexOf(currentSence) == -1)){ //避免重复添加
					
					history += (history.length() > 0 ? ";" : "") + currentSence;
					meno = sceneItem.getHistoryContent();
				}
				
                sceneItem.setFocus(true);
                currentScene = sceneItem;
			}
		}
		
		scriptIndex = (int) (skipScriptIndex == null ? 0 : skipScriptIndex);
		
		return currentScene;
	}
	
    
    public List<Scene> initSceneStore(){
    	
    	sceneStore.add(start_1Scene());
    	sceneStore.add(station_2Scene());
    	sceneStore.add(restaurant_3Scene());
    	sceneStore.add(jianxing_4Scene());
    	sceneStore.add(assistant_office_5Scene());
    	sceneStore.add(painter_home_6Scene());
    	sceneStore.add(company_dorm_7Scene());
    	sceneStore.add(techer_dorm_8Scene());
    	sceneStore.add(pear12_9Scene());
    	sceneStore.add(children_place_10Scene());
    	sceneStore.add(painter_home_11Scene());
    	sceneStore.add(village_12Scene());
    	sceneStore.add(dorm_place_13Scene());
    	sceneStore.add(police_station_14Scene());
    	sceneStore.add(company_dorm_15Scene());
    	sceneStore.add(ren_boos_home_16Scene());
    	sceneStore.add(envelope_17Scene());
    	sceneStore.add(computer_room_18Scene());
    	sceneStore.add(government_place_19Scene());
    	sceneStore.add(end_20Scene());
        
        return sceneStore;
    }
    
    private Scene start_1Scene(){
        List<Script> focusScriptLst = new ArrayList<Script>();
        List<Script> blurScriptLst = new ArrayList<Script>();
    
        focusScriptLst.add(new Script(1L,"Scene_Start_1",1L,"tiper_light","天空是蓝的，所以女人就有秘密。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(2L,"Scene_Start_1",2L,"police","王磊没能赶上女友李梓的葬礼，不过他把我们的见面地点约在了火车站。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(3L,"Scene_Start_1",3L,"police","这是一个人潮汹涌的拥挤地方啊，不过作为一名有内涵有水平的警察叔叔，有时顺从伤心人的心意也是一种美德吧。",null,null,null,null,null,null,null));
        
        blurScriptLst.add(new Script(1L,"Scene_Start_1",1L,"tiper_harry","一出火车站能看到一座横过的天桥，其两侧有约1米高的铁栅护栏，边上种了些茎叶茂盛的矮小灌木。",null,null,null,null,null,null,null));
        blurScriptLst.add(new Script(2L,"Scene_Start_1",2L,"tiper_harry","叶子的正面是叶绿色，而背光面却是呈绛紫色，不细看的话会误以为那是一片片花瓣。",null,null,null,null,null,null,null));
        blurScriptLst.add(new Script(3L,"Scene_Start_1",3L,"tiper_light","“告诉您一个秘密，它的名字叫三角梅，据说是中国南方某知名城市的市花。。。哦，您知道是什么地方了。。。”",null,null,null,null,null,null,null));
        
        return new Scene(1L,"Scene_Start_1","序幕","Start",null,"Scene_Station_2",null,focusScriptLst,blurScriptLst,"三角梅？市花？南方城市？有百度无难度哈","王磊出差回来，会面地方约在火车站",null,null,null,null,null,null,false);
    }
   
    private Scene station_2Scene(){
        List<Script> focusScriptLst = new ArrayList<Script>();
        List<Script> blurScriptLst = new ArrayList<Script>();
        
        focusScriptLst.add(new Script(1L,"Scene_Station_2",1L, "wonglei","在五天前，王磊接到了警方的电话，而他正好在别的城市忙于分公司的一个工程项目。",null,null,null,null,null,null,null));
        focusScriptLst.add(new Script(2L,"Scene_Station_2",2L,"police","关于王磊在我的案情记录本上，对他的身份描述只是简单的一句：男，27岁，软件工程师。",null,null,null,null,null,null,null));
        focusScriptLst.add(new Script(3L,"Scene_Station_2",3L,"tiper_harry","死者名叫李梓，生前是本市重点中学教师，命运多舛的她在七岁时，父母双双死于一场交通意外，三年后她被一对姨父母从孤儿院领回收养。",null,null,null,null,null,null,null));
        focusScriptLst.add(new Script(4L,"Scene_Station_2",4L,"wonglei","“嗯，警官”，王磊无趣地打个招呼",null,null,null,null,null,null,null));
        focusScriptLst.add(new Script(5L,"Scene_Station_2",5L,"police","“我们去喝一杯，您觉得怎么样？”，我知道他从外地赶回，还饿着肚子。",null,null,null,null,null,null,null));
        focusScriptLst.add(new Script(6L,"Scene_Station_2",6L,"wonglei","“好的，我们去那边的WWW快餐店坐下聊聊”，王磊显然很乐意。",null,null,null,null,null,null,null));

        blurScriptLst.add(new Script(1L,"Scene_Station_2",1L,"tiper_light","“每个城市都有一个神秘的地方叫车站，它能帮你实现心底的愿望。。。只要你愿意向这个城市抵押自己的灵魂，但是所有来到这里的人都将付出难以想象的代价。。。”",null,null,null,null,null,null,null));
        blurScriptLst.add(new Script(2L,"Scene_Station_2",2L,"tiper_light","“好无聊的传说”，突然间阴风阵阵，“我怎么冒失地来到这鬼地方，还是快点离开吧”",null,null,null,null,null,null,null));

        return new Scene(2L,"Scene_Station_2","火车站","Station","Scene_Start_1","Scene_Restaurant_3",null,focusScriptLst,blurScriptLst,"世上本没有WWW快餐店的，吃的人多了自然就有了","王磊和我在火车站见面，然后到WWW快餐店用餐",null,null,null,null,null,null,false);
    }    
    
    private Scene restaurant_3Scene(){
        List<Script> focusScriptLst = new ArrayList<Script>();
        List<Script> blurScriptLst = new ArrayList<Script>();

        focusScriptLst.add(new Script(1L,"Scene_Restaurant_3",1L,"tiper_harry","王磊要了一大杯加冰块的可乐，用吸管搅拌几下，猛地吸一大口。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(2L,"Scene_Restaurant_3",2L,"wonglei","“呵，人心情不好的时候，可以吃点甜的东西，可以让自己精神些”，王磊自嘲地笑一声。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(3L,"Scene_Restaurant_3",3L,"wonglei","“到了寒暑假，她喜欢到郊区采风，把调色板、颜料、画布和画框什么的打好包，然后一个人骑着自行车到郊外。”",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(4L,"Scene_Restaurant_3",4L,"wonglei","“还好，我每次都是她作品的第一位观众”",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(5L,"Scene_Restaurant_3",5L,"police","走出快餐店，我有点感到沮丧，李梓是的确是一个平凡的女孩子啊。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(6L,"Scene_Restaurant_3",6L,"police","事实上在对案发现场进行的取证，对死者生前的人际关系、生活环境的调查之后，警方对李梓死亡案件的定性倾向于自杀。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(7L,"Scene_Restaurant_3",7L,"wonglei","“明天我回公司要请上一段时间的假”，最后王磊抛下一句。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(8L,"Scene_Restaurant_3",8L,"police","我知道他就职于本市一家名叫嘉兴实业的公司。",null,null,null,null,null,null,null) );

        blurScriptLst.add(new Script(1L,"Scene_Restaurant_3",1L,"storer","“欢迎光临本店！”",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(2L,"Scene_Restaurant_3",2L,"storer","“上个星期的星期八您吃了一碗水饺，一掷三元五毛的付钱态度，就象黑暗中的一只小小萤火虫，让本店蓬壁生辉不已”",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(3L,"Scene_Restaurant_3",3L,"storer","“我们最喜欢做熟客生意，总之宰一个算一个，很快我们可以在北京城的华盛顿特区开分店了”",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(4L,"Scene_Restaurant_3",4L,"tiper_harry","无语吧。。。",null,null,null,null,null,null,null) );

        return new Scene(3L,"Scene_Restaurant_3","WWW快餐店","Restaurant","Scene_Station_2","Scene_Jianxing_4",null,focusScriptLst,blurScriptLst,"出差用餐可以找公司报销的，聪明的你接下来可以去。。。","在快餐店用餐，王磊打算明天回嘉兴实业请假",null,null,null,null,null,null,false);
   }
    
    private Scene jianxing_4Scene(){
        List<Script> focusScriptLst = new ArrayList<Script>();
        List<Script> blurScriptLst = new ArrayList<Script>();

        focusScriptLst.add(new Script(1L,"Scene_Jianxing_4",1L,"guangxianyuan","“磊磊，有你的包裹”",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(2L,"Scene_Jianxing_4",2L,"guangxianyuan","王磊刚进办公室，职员关小原递来一邮包，包里淡淡的散发出油墨、松节油气味。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(3L,"Scene_Jianxing_4",3L,"guangxianyuan","“磊磊，对不起啊。其实包裹在五天前寄过来了，可是您当时在分公司出差”，关小原说道。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(4L,"Scene_Jianxing_4",4L,"wonglei","“嗯嗯”，王磊拆开了包裹是有一副油画，从邮戳日期来看，这副迟来的油画可以说是她的遗作了。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(5L,"Scene_Jianxing_4",5L,"tiper_harry","油画上是一株株开着红花的植物，叶子青绿细且长，密密麻麻地团簇着一枝高高的茎杆，枝头顶抽出四、五朵胭红色的花蕾，每朵六瓣，上方的三瓣稍大，如同一个个精美的喇叭。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(6L,"Scene_Jianxing_4",6L,"tiper_harryt","画的背景有一条公路，拐了个弯消失在一个小山丘之后，在远处是个水湖，隐约有一排水车和一座小桥。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(7L,"Scene_Jianxing_4",7L,"tiper_harry","一副别致的风景画，也许是李梓在采风写生时妙手偶得。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(8L,"Scene_Jianxing_4",8L,"guangxianyuan","“好漂亮的油画，画的是剑兰喔！”，关小原凑过来嚷道，“磊磊，下班前你到助理办公室填写一份请假单吧”",null,null,null,null,null,null,null) );

        blurScriptLst.add(new Script(1L,"Scene_Jianxing_4",1L,"guangxianyuan","“磊磊，这么快就回来做事了”",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(2L,"Scene_Jianxing_4",2L,"guangxianyuan","“嗯嗯，虽然新劳动法规定存在有薪假期，但是别指望在假期里正常上班会有2倍的工资，至少公司不强制我们休假已经够仁慈的了。。。”",null,null,null,null,null,null,null) );	
        blurScriptLst.add(new Script(3L,"Scene_Jianxing_4",3L,"guangxianyuan","“什么？靠加班费赚钱？这只是一个都市传说。。。”",null,null,null,null,null,null,null) );

        return new Scene(4L,"Scene_Jianxing_4","嘉兴实业","Jianxing","Scene_Restaurant_3","Scene_Assistant_Office_5",null,focusScriptLst,blurScriptLst,"什么？什么？你不知道公司编号为ABC1099的马桶在助理办公室","王磊收到了李梓的油画，他要到助理办公室拿请假单",null,null,null,null,null,null,false);
    }

    private Scene assistant_office_5Scene(){
        List<Script> focusScriptLst = new ArrayList<Script>();
        List<Script> blurScriptLst = new ArrayList<Script>();

        focusScriptLst.add(new Script(1L,"Scene_Assistant_Office_5",1L,"guangxianyuan","“磊磊，恋爱了？”，关小原笑看着满脸疑惑的王磊。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(2L,"Scene_Assistant_Office_5",2L,"guangxianyuan","“就知道您们男生不细心，一会您查下剑兰的花语”",null,null,null,null,null,null,null) );        
        focusScriptLst.add(new Script(3L,"Scene_Assistant_Office_5",3L,"guangxianyuan","“磊磊，你知道我们部门科长，穿的是什么牌子皮鞋吗？是‘天堂鸟’耶，知道它的花语不？是‘为恋爱而打扮得漂漂亮亮的男子’”，关小原调侃道。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(4L,"Scene_Assistant_Office_5",4L,"wonglei","王磊填写完假单后，想起公司的附近有个叫无二斋画廊，打算把油画装一个相框。",null,null,null,null,null,null,null) );

        blurScriptLst.add(new Script(1L,"Scene_Assistant_Office_5",1L,"guangxianyuan","“对于花我只想要两样，一样叫‘有钱花’，另一样叫‘随便花’”。",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(2L,"Scene_Assistant_Office_5",2L,"guangxianyuan","“呵呵。。。”",null,null,null,null,null,null,null) );

        return new Scene(5L,"Scene_Assistant_Office_5","助理办公室","Assistant_Office","Scene_Jianxing_4","Scene_Painter_Home_6",null,focusScriptLst,blurScriptLst,"唔唔，卢浮宫里的蒙娜丽莎画边上，由四根木条搭成的结构叫什么呢？","王磊找关小原填写请假单，打算去无二斋画廊装个相框",null,null,null,null,null,null,false);
    }
        
    private Scene painter_home_6Scene(){
        List<Script> focusScriptLst = new ArrayList<Script>();
        List<Script> blurScriptLst = new ArrayList<Script>();

        focusScriptLst.add(new Script(1L,"Scene_Painter_Home_6",1L,"tiper_harry","下班后，王磊去了公司附近的无二斋画廊。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(2L,"Scene_Painter_Home_6",2L,"storer","店里有专门为艺术、书画爱好者，提供专业的装裱服务，店员经验丰富，工作认真，而且店主本身就是知名的丹青高手。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(3L,"Scene_Painter_Home_6",3L,"storer","可惜店主外出办事，王磊便把油画交给店员，约定一天后来取货。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(4L,"Scene_Painter_Home_6",4L,"tiper_harry","事情交待完毕，王磊便回公司的宿舍。",null,null,null,null,null,null,null) );

        blurScriptLst.add(new Script(1L,"Scene_Painter_Home_6",1L,"storer","“猪八戒回高老庄，身边是按摩女郎；唐僧咬着方便面，到大街上给人道个吉祥；沙和尚驾着摩托艇把鱼儿打个精光。。。”",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(2L,"Scene_Painter_Home_6",2L,"storer","“不好意思，本店生意实在难做，我打算练几首 RhythmBlues，找李单江先生一起投身娱乐圈”",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(3L,"Scene_Painter_Home_6",3L,"storer","“看看刚才您想问什么？是 RB！”",null,null,null,null,null,null,null) );

        return new Scene(6L,"Scene_Painter_Home_6","无二斋画廊","Painter_Home","Scene_Assistant_Office_5","Scene_Company_Dorm_7",null,focusScriptLst,blurScriptLst,"“生命应该拥着最爱归家，生活别过份童话化”。。。晕！忙了一天你居然想到去唱 K ","王磊到无二斋画廊，之后便回去公司宿舍",null,null,null,null,null,null,false);
    }
        
    private Scene company_dorm_7Scene(){
        List<Script> focusScriptLst = new ArrayList<Script>();
        List<Script> blurScriptLst = new ArrayList<Script>();

        focusScriptLst.add(new Script(1L,"Scene_Company_Dorm_7",1L,"tiper_harry","回到公司宿舍大约是晚上九点半钟，这个时候是电视台的黄金新闻时段。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(2L,"Scene_Company_Dorm_7",2L,"tiper_harry","北方一城市发生特大自然灾害，本市社会各界纷纷慷慨解囊支持灾区。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(3L,"Scene_Company_Dorm_7",3L,"liu","然而本地一家实力雄厚，叫名石房产公司老板柳河东，当众捐出一元钱。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(4L,"Scene_Company_Dorm_7",4L,"liu","当他面对手持话筒、表情诧异的记者，解释说“死人的事情经常有，灾害发生是常态性的”，",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(5L,"Scene_Company_Dorm_7",5L,"liu","还透露公司内部慈善的募捐活动中，有条提示：每次募捐，普通员工的捐款以一元为限，其意就是不要慈善成为负担。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(6L,"Scene_Company_Dorm_7",6L,"wonglei","王磊无语地关掉电视，心想明天去看下李梓的遗物，走走两人曾经一起去过的地方。",null,null,null,null,null,null,null) );

        blurScriptLst.add(new Script(1L,"Scene_Company_Dorm_7",1L,"tiper_harry","“一只...”",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(2L,"Scene_Company_Dorm_7",2L,"tiper_harry","“两只...”",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(3L,"Scene_Company_Dorm_7",3L,"tiper_harry","“三只...”",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(4L,"Scene_Company_Dorm_7",4L,"tiper_harry","“金窝银窝不如自己的狗窝，拉灯睡觉”",null,null,null,null,null,null,null) );

	return new Scene(7L,"Scene_Company_Dorm_7","公司宿舍","Company_Dorm","Scene_Painter_Home_6","Scene_Techer_Dorm_8",null,focusScriptLst,blurScriptLst,"填空题：您累计去男/女朋友的住处 ____次?","电视里播放名石房产柳河东“一元捐”新闻事件，王磊决定明天去学校的李梓房间",null,null,null,null,null,null,false);
    }
    
    private Scene techer_dorm_8Scene(){
        List<Script> focusScriptLst = new ArrayList<Script>();
        List<Script> blurScriptLst = new ArrayList<Script>();

        focusScriptLst.add(new Script(1L,"Scene_Techer_Dorm_8",1L,"wonglei","在学校宿管科，王磊对看护宿舍的胖阿姨说想看下李梓房间。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(2L,"Scene_Techer_Dorm_8",2L,"mather","这是一位大约四十来岁，体形稍胖的中年妇女，表情惊讶地盯着他的脸，迟疑了好一会，才拿过挂在墙壁的一大串锁匙。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(3L,"Scene_Techer_Dorm_8",3L,"tiper_harry","房间采光度非常好，有一个很大的窗户，窗外面没有下水道水管之类可以作攀缘的物件。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(4L,"Scene_Techer_Dorm_8",4L,"tiper_harry","校方考虑到李梓养父母的悲痛情绪，近期房间不会腾空作它用的，所以衣物、书桌等摆设依然如旧。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(5L,"Scene_Techer_Dorm_8",5L,"tiper_harry","中午的校园很安静，窗外一片阳光灿烂，偶尔有一两个学生快步走过，王磊神情黯然地走出宿舍楼。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(6L,"Scene_Techer_Dorm_8",6L,"ano","突然从后面跑来一名少年，热切地说道：“我想告诉你一些与李梓老师相关的事儿，一会你到十二颗梧桐处等我吧”",null,null,null,null,null,null,null) ); 

        blurScriptLst.add(new Script(1L,"Scene_Techer_Dorm_8",1L,"tiper_light","“嘘!这是犯罪现场!”",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(2L,"Scene_Techer_Dorm_8",2L,"tiper_light","“杀手的第一条守则是：千万别回犯罪现场去了!”",null,null,null,null,null,null,null) );
	
        return new Scene(8L,"Scene_Techer_Dorm_8","教师宿舍","Techer_Dorm","Scene_Company_Dorm_7","Scene_Pear12_9",null,focusScriptLst,blurScriptLst,"二十四桥明月夜，梧桐树下六加七再减一","王磊去了李梓房间，有一名少年告知他到十二颗梧桐处等候，有要事相告",null,null,null,null,null,null,false);
    }
    
    private Scene pear12_9Scene(){
        List<Script> focusScriptLst = new ArrayList<Script>();
        List<Script> blurScriptLst = new ArrayList<Script>();

        focusScriptLst.add(new Script(1L,"Scene_Pear12_9",1L,"ano","“请叫我阿诺，刚才和您说话的舍管阿姨是我妈”，少年解释道，“那我直说吧，李梓老师的死是一场内有隐情的谋杀”",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(2L,"Scene_Pear12_9",2L,"ano","“在李梓老师出事的前几天，我正好在楼下舍监小房帮我妈看宿舍。看见李老师上了一辆小汽车，大约两个小时后，她神色慌张地跑回来”",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(3L,"Scene_Pear12_9",3L,"ano","“车牌号是‘SA001’，您可以去查下车辆的主人”",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(4L,"Scene_Pear12_9",4L,"wonglei","“谋杀！”，王磊吃惊地望着阿诺，而少年的镇定神情似乎是向面前的成年人表示，这绝非青春期荷尔蒙激素或肾上腺分泌物过多作怪。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(5L,"Scene_Pear12_9",5L,"tiper_harry","不管真相如何，王磊现在的脑袋一片空白，接下来他去了儿童福利院。",null,null,null,null,null,null,null) );

        blurScriptLst.add(new Script(1L,"Scene_Pear12_9",1L,"tiper_light","“这破地方居然叫‘十二颗梧桐’” ，其实这里没有一棵梧桐树也没有，只是学校操场边上的一个小草坪。",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(2L,"Scene_Pear12_9",2L,"tiper_light","“世间上的事情道理是相同的，‘老婆饼’里根本吃不出‘老婆’来”",null,null,null,null,null,null,null) ); 

        return new Scene(9L,"Scene_Pear12_9","十二颗梧桐","Pear12","Scene_Techer_Dorm_8","Scene_Children_Place_10",null,focusScriptLst,blurScriptLst,"填空题：您和男/女朋友常去的地方是  ____?","少年阿诺提供了一个‘SA001’的车牌号，王磊计划去儿童福利院",null,null,null,null,null,null,false);
    }
    
    private Scene children_place_10Scene(){
        List<Script> focusScriptLst = new ArrayList<Script>();
        List<Script> blurScriptLst = new ArrayList<Script>();
	
        focusScriptLst.add(new Script(1L,"Scene_Children_Place_10",1L,"wonglei","在路边的糖果店铺，王磊要了一小牛皮纸袋的如皋小吃董糖，便跳上去儿童福利院的公交。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(2l,"Scene_Children_Place_10",2L,"tiper_harry","李梓在双亲遇难后，有三年的时间是在儿童福利院度过的，所以她对这里的有感情，经常和王磊一起过来做义工。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(3l,"Scene_Children_Place_10",3L,"dean","福利院院长知道李梓的过世消息，两人相互劝勉几句。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(4l,"Scene_Children_Place_10",4L,"ren","这时一位特殊的客人来到办公室，他正是三和集团老总任是非，院长向任是非介绍王磊。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(5l,"Scene_Children_Place_10",5L,"ren","任是非听到“李梓”的名字，脸上露出一丝悲戚，不过很快便镇定下来，伸出双手热情有力地和王磊握手。",null,null,null,null,null,null,null) );

        blurScriptLst.add(new Script(1L,"Scene_Children_Place_10",1L,"tiper_light","“如果你喜欢我，就抱抱我；如果你喜欢我，就亲亲我”",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(2L,"Scene_Children_Place_10",2L,"dean","“年轻人来做义工吧，孩子需要您的关爱”",null,null,null,null,null,null,null) );
        
    	return new Scene(10L,"Scene_Children_Place_10","儿童福利院","Children_Place","Scene_Pear12_9","Scene_Painter_Home_11",null,focusScriptLst,blurScriptLst,"不会吧，你忘记了和店员的约定？嗯，现在还来得及哈","在儿童福利院里王磊认识了任是非，明天到无二斋的画室取画",null,null,null,null,null,null,false);
    }
    
    private Scene painter_home_11Scene(){
        List<Script> focusScriptLst = new ArrayList<Script>();
        List<Script> blurScriptLst = new ArrayList<Script>();
	
        focusScriptLst.add(new Script(1L,"Scene_Painter_Home_11",1L,"tiper_harry","第二天早上，王磊来到无二斋画廊。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(2L,"Scene_Painter_Home_11",2L,"boss","“你是在清木村取的景吧？”老板问道，“这个小村庄风景宜人，山水之间孕育着天工的神秀，而草木一针一叶洋溢着天地的灵气”",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(3L,"Scene_Painter_Home_11",3L,"boss","“你看这几朵胭红色的花，堪如神来之笔，在一片安静平和的周遭，却令人意外地升华出‘红杏枝头春意闹’意境，它们的生命力几乎让我想起梵高的名作《响日葵》。”",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(4L,"Scene_Painter_Home_11",4L,"boss","“在大胆抒发内心臆想的同时，又很好地达到色彩上的表现力和画面的寓意性”",null,null,null,null,null,null,null) ); 
        focusScriptLst.add(new Script(5L,"Scene_Painter_Home_11",5L,"tiper_light","如果没有这位言谈过于自恋的艺术家，那么我们这个案子显得未免太单调、太无情趣了。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(6L,"Scene_Painter_Home_11",6L,"wonglei","“这是我一个朋友的作品”，王磊忍不住打断他的话，“你能重复一下刚才您说的村子名吗？”",null,null,null,null,null,null,null) ); 
        focusScriptLst.add(new Script(7L,"Scene_Painter_Home_11",7L,"boss","老板听罢，脸上掠过一丝尴尬，说了三个字“清木村”。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(8L,"Scene_Painter_Home_11",8L,"boss","“市政府礼堂要举办清木村房产投标工程，那个小村庄怕是很快荡然无存了”，老板满脸遗憾地说道。",null,null,null,null,null,null,null) );

        blurScriptLst.add(new Script(1L,"Scene_Painter_Home_11",1L,"boss","“唐代诗人画家王维说过‘肇自然之性，成造化之功。或咫尺之图，写百里之景，东西南北，宛尔目前。春夏秋冬生于笔下’，意思是说画家在绘画时，内心要有包罗万象之酝酿，下笔力求天马行空之酣然淋沥。。。”",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(2L,"Scene_Painter_Home_11",2L,"boss","“年轻人来学画画吧，既然我们这么有缘，学费就给您打八折哈”",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(3L,"Scene_Painter_Home_11",3L,"storer","“亲，你的左耳还好吗？。。。”",null,null,null,null,null,null,null) );

    	return new Scene(11L,"Scene_Painter_Home_11","无二斋画廊","Painter_Home","Scene_Children_Place_10","Scene_Village_12",null,focusScriptLst,blurScriptLst,"亲，你知道二元钱钞票的背景图案，画的是哪里吗？绝对不是游戏里的清木村","王磊从画廊老板处获悉清木村",null,null,null,null,null,null,false);
    }
    
    private Scene village_12Scene(){
        List<Script> focusScriptLst = new ArrayList<Script>();
        List<Script> blurScriptLst = new ArrayList<Script>();
	
        focusScriptLst.add(new Script(1L,"Scene_Village_12",1L,"tiper_harry","去清木村约花了四十多分钟，王磊在村口下了车。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(2L,"Scene_Village_12",2L,"tiper_harry","远处的小水湖、几架凭水力缓慢地翻转着的水车，还有一架小桥，这里的风景与油画里的一模一样。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(3L,"Scene_Village_12",3L,"wonglei","失望的是这里是真的没有开着胭红色花蕾的剑兰。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(4L,"Scene_Village_12",4L,"wonglei","他想起无二斋老板的话“天马行空”、“神来之笔”，不禁苦笑。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(5L,"Scene_Village_12",5L,"tiper_harry","王磊收到阿诺发来的手机短信，说查出车子的主人，明天请到学校的宿管科找他。\n\n最后王磊上了一辆回城的公交车。",null,null,null,null,null,null,null) );

        blurScriptLst.add(new Script(1L,"Scene_Village_12",1L,"tiper_light","“水木清华，婉兮清扬”",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(2L,"Scene_Village_12",2L,"tiper_light","清木村，传说中本市的最后一处世外桃源。",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(3L,"Scene_Village_12",3L,"tiper_light","“据说名石房产和三和集团都在打这一块地皮的主意，不知道哪一家会占得先机”",null,null,null,null,null,null,null) );

    	return new Scene(12L,"Scene_Village_12","清木村","Village","Scene_Painter_Home_11","Scene_Dorm_Place_13",null,focusScriptLst,blurScriptLst,"亲，阿诺的妈妈在哪里？","清木村的风景和油画所绘相似；收到阿诺的短信，告知明天到学校的宿管处",null,null,null,null,null,null,false);
    }
    
    private Scene dorm_place_13Scene(){
        List<Script> focusScriptLst = new ArrayList<Script>();
        List<Script> blurScriptLst = new ArrayList<Script>();

        focusScriptLst.add(new Script(1L,"Scene_Dorm_Place_13",1L,"tiper_harry","翌日中午，王磊来到了学校。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(2L,"Scene_Dorm_Place_13",2L,"mather","胖胖的舍管阿姨隔窗一瞅见他，情绪变得激动，哭喊着用两手死死地抱住王磊的大腿。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(3L,"Scene_Dorm_Place_13",3L,"tiper_harry","闻讯跑来的门卫和师生们，将王磊团团地围住，最后把他扔上警车。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(4L,"Scene_Dorm_Place_13",4L,"ano","此时阿诺躺在医院的重症监护室(ICU)，接受医生的进一步观察。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(5L,"Scene_Dorm_Place_13",5L,"mather","李梓的自杀，儿子与这位陌生人交谈后便出事，可怜的母亲已将王磊视为危险人物，至少是一个不祥的晦气家伙。",null,null,null,null,null,null,null) );

        blurScriptLst.add(new Script(1L,"Scene_Dorm_Place_13",1L,"mather","“我儿子身体康复了，现在他比以前更聪明可爱”",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(2L,"Scene_Dorm_Place_13",2L,"police","在我接触的一些案子中，经常碰到一些有意思的人，",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(3L,"Scene_Dorm_Place_13",3L,"police","他们在案件中的角色仅仅是一个“出来打酱油”的旁观者，",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(4L,"Scene_Dorm_Place_13",4L,"police","甚至是一名“叼着雪糕”漫不经心、匆匆而过的路人，",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(5L,"Scene_Dorm_Place_13",5L,"police","他们在过于丰富的想像力和好奇心驱动下，往往着眼于一些经不起推敲的蛛丝马迹，",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(6L,"Scene_Dorm_Place_13",6L,"police","经无限放大“成功地”还原成一个个事件的“内幕”，",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(7L,"Scene_Dorm_Place_13",7L,"police","至少是符合他们“逻辑”的“内幕”。",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(8L,"Scene_Dorm_Place_13",8L,"police","他们很聪明，知道“狗不叫，是因为它和罪犯很熟”，",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(9L,"Scene_Dorm_Place_13",9L,"police","而隔壁“正在浇花，面貌和善”的老头，他们第一时间就能认定其绝对是凶手。",null,null,null,null,null,null,null) );

    	return new Scene(13L,"Scene_Dorm_Place_13","宿管处","Dorm_Place","Scene_Village_12","Scene_Police_Station_14",null,focusScriptLst,blurScriptLst,"好人在天堂，坏人通过在 XX局，由 XX叔叔调教","王磊被送进警察局，阿诺躺在医院里接受治疗",null,null,null,null,null,null,false);
    }
    
    private Scene police_station_14Scene(){
        List<Script> focusScriptLst = new ArrayList<Script>();
        List<Script> blurScriptLst = new ArrayList<Script>();

        focusScriptLst.add(new Script(1L,"Scene_Police_Station_14",1L,"police","我和王磊的第二次见面是在警局，嘴里在咀嚼着东西，偶尔发出“咯咯”响声。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(2L,"Scene_Police_Station_14",2L,"police","人心情不好的时候，可以吃点甜的东西，可以让自己精神些。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(3L,"Scene_Police_Station_14",3L,"police","警方查出车子的主人，是名石房产开发公司柳河东，接下来我们的工作，是要考虑对他进行布控。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(4L,"Scene_Police_Station_14",4L,"tiper_harry","在警察局，王磊并没有遇到很大的麻烦。",null,null,null,null,null,null,null) );

        blurScriptLst.add(new Script(1L,"Scene_Police_Station_14",1L,"tiper_light","“警察局可不是谁都能进的，你家里有没有钱？够不够资格犯罪先？不过看您委琐中略带几分自信，这可是做山贼的必要条件喔！”",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(2L,"Scene_Police_Station_14",2L,"tiper_light","“其实我是一个临时工”",null,null,null,null,null,null,null) );

    	return new Scene(14L,"Scene_Police_Station_14","警察局","Police_Station","Scene_Dorm_Place_13","Scene_Company_Dorm_15",null,focusScriptLst,blurScriptLst,"“生命应该拥着最爱归家，生活别过份童话化”。。。歌声又响起了","王磊在警察局，获知车子主人是名石房产柳河东的",null,null,null,null,null,null,false);
    }
    
    private Scene company_dorm_15Scene(){
        List<Script> focusScriptLst = new ArrayList<Script>();
        List<Script> blurScriptLst = new ArrayList<Script>();

        focusScriptLst.add(new Script(1L,"Scene_Company_Dorm_15",1L,"ren","当王磊回到公司宿舍时，发现一名司机在门口已等候多时，说三和集团“任是非先生有请”。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(2L,"Scene_Company_Dorm_15",2L,"wonglei","王磊回忆在儿童福利院的一面之缘，便一起乘车到任是非住处。",null,null,null,null,null,null,null) );

        blurScriptLst.add(new Script(1L,"Scene_Company_Dorm_15",1L,"guangxianyuan","“公司福利好待遇高，只收水电费用，一个月不到50元。。。”",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(2L,"Scene_Company_Dorm_15",2L,"guangxianyuan","“什么？女仆服务。。。”",null,null,null,null,null,null,null) );

        return new Scene(15L,"Scene_Company_Dorm_15","公司宿舍","Company_Dorm","Scene_Police_Station_14","Scene_Ren_Boos_Home_16",null,focusScriptLst,blurScriptLst,"“BOSS 级怪兽的宫殿","王磊随车至任是非住处",null,null,null,null,null,null,false);
    }
    
    private Scene ren_boos_home_16Scene(){
        List<Script> focusScriptLst = new ArrayList<Script>();
        List<Script> blurScriptLst = new ArrayList<Script>();

        focusScriptLst.add(new Script(1L,"Scene_Ren_Boos_Home_16",1L,"ren","“在出席清木村的公路贯通仪式结束后，我得意忘形地驾车游玩，在村口公路的拐弯处有个孩子跑下来，而我已经来不及踩刹车，车子。。。”",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(2L,"Scene_Ren_Boos_Home_16",2L,"ren","“一场悲剧发生时，李梓在小山丘上画画，目睹到车祸的经过。。。”",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(3L,"Scene_Ren_Boos_Home_16",3L,"tiper_harry","原来油画隐藏一个车祸秘密，画的是案发地点景象。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(4L,"Scene_Ren_Boos_Home_16",4L,"tiper_harry","如果说剑兰青翠的叶子枝枝刚毅，傲然立于山巅，飘逸如花中之侠，那么被茎叶保护着的花蕾，宛如一颗廋弱无力、自我封闭的心灵。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(5L,"Scene_Ren_Boos_Home_16",5L,"ren","“这封是李梓的遗书，有一件事恐怕得麻烦你，是破解一份电脑文件的密码，你可以利用电脑室的任何设备”",null,null,null,null,null,null,null) );

        blurScriptLst.add(new Script(1L,"Scene_Ren_Boos_Home_16",1L,"ren","人生苦短",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(2L,"Scene_Ren_Boos_Home_16",2L,"ren","去日不多矣",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(3L,"Scene_Ren_Boos_Home_16",3L,"ren","年轻人我们干了这杯",null,null,null,null,null,null,null) );

        return new Scene(16L,"Scene_Ren_Boos_Home_16","任宅","Ren_Boos_Home","Scene_Company_Dorm_15","Scene_Computer_Room_18","Scene_Envelope_17",focusScriptLst,blurScriptLst,"天空是蓝的，所以女人就有秘密","李梓是自杀，王磊阅读信件，到电脑室破解文件的密码",null,null,null,null,null,null,false);
    }
    
    private Scene envelope_17Scene(){
        List<Script> focusScriptLst = new ArrayList<Script>();
        List<Script> blurScriptLst = new ArrayList<Script>();

        focusScriptLst.add(new Script(1L,"Scene_Envelope_17",1L,"tiper_harry","任是非取出一封信，上面是熟悉的李梓亲笔字迹：",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(2L,"Scene_Envelope_17",2L,"lizi","我决定去一个很远的地方，那里是远离尘嚣的净土，没有痛苦和忧伤。身边的人不要担心，请在心里为我的解脱感到宽慰吧。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(3L,"Scene_Envelope_17",3L,"lizi","感谢您和院长妈妈对我的关爱和呵护，此生无以回报。我有一份名石房产的清木村房产项目投标书，是我在答应做柳河东家的孩子家庭教师时。。。窃取的，希望它可以对你的事业有帮助。",null,null,null,null,null,null,null) );

        blurScriptLst.add(new Script(1L,"Scene_Envelope_17",1L,"tiper_harry","爱是恒久忍耐、又有恩慈；爱是不嫉妒，爱是不自夸，不张狂，不做害羞的事，不求自己的益处，不轻易发怒，",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(2L,"Scene_Envelope_17",2L,"tiper_harry","不计算人的恶，不喜欢不义，只喜欢真理；凡事包容，凡事相信，凡事盼望，凡事忍耐；爱是永不止息！--(圣经<<哥林多前书>>)",null,null,null,null,null,null,null) );

        return new Scene(17L,"Scene_Envelope_17","李梓的信件","","Scene_Ren_Boos_Home_16","Scene_Computer_Room_18",null,focusScriptLst,blurScriptLst,"请到电脑室...","王磊阅读李梓的信件，到电脑室破解文件的密码",null,null,null,null,null,null,false);
    }
    
    private Scene computer_room_18Scene(){
        List<Script> focusScriptLst = new ArrayList<Script>();
        List<Script> blurScriptLst = new ArrayList<Script>();

        focusScriptLst.add(new Script(1L,"Scene_Computer_Room_18",1L,"tiper_harry","信封里掉出一块小SD卡，是我们平时用在手机里的存储卡，",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(2L,"Scene_Computer_Room_18",2L,"tiper_harry","只要插进一个读卡器，就可以象普通的U盘一样，在电脑里读取和储存文件。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(3L,"Scene_Computer_Room_18",3L,"lizi","想必在会面的时候，李梓趁着柳河东的疏忽大意，把他电脑里的资料做份拷贝。",null,null,null,null,null,null,null) );        
        /*focusScriptLst.add(new Script(4L,"Scene_Computer_Room_18",4L,"tiper_harry","标书是一份加密的文件，阅读它之前需要输入正确的口令。",
                new Question(1L, "PassWord", "请输入文档密码", "=", "001", null, null, null, null, null, null),
                null,null,null,null,null,null) );*/
        
        blurScriptLst.add(new Script(1L,"Scene_Computer_Room_18",1L,"tiper_light","“嗯嗯，SA001.。。SA001.。。”",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(2L,"Scene_Computer_Room_18",2L,"tiper_light","“亲，凡是用自己的XX号码做密码的，都是一个好人哈”",null,null,null,null,null,null,null) );

        return new Scene(18L,"Scene_Computer_Room_18","电脑室","Computer_Room","Scene_Ren_Boos_Home_16","Scene_Government_Place_19",null,focusScriptLst,blurScriptLst,"“亲，其实我的银行密码是我的生日，不过您知道我的生日是哪一天吗？","破解名石房产在清木村项目的标书的文件密码",null,null,null,null,null,null,false,
        					new Question(1L, "PassWord", "请输入文档密码", "=", "001", null, null, null, null, null, null)
        );
    }
    
    private Scene government_place_19Scene(){
        List<Script> focusScriptLst = new ArrayList<Script>();
        List<Script> blurScriptLst = new ArrayList<Script>();

        focusScriptLst.add(new Script(1L,"Scene_Government_Place_19",1L,"tiper_harry","在市政府礼堂，本市规模最大“经济适用房”房产项目投标会，暨清木村城村改造工程如期进行。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(2L,"Scene_Government_Place_19",2L,"tiper_harry","当名石房产公司出示的标价，同项目标底一分不差、不多不少完全相同时，全场不禁一遍哗然。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(3L,"Scene_Government_Place_19",3L,"tiper_harry","不过主持人马上宣布，此次中标者是三和集团，以低于名石房产公司一元钱的标价，获得项目的承建权。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(4L,"Scene_Government_Place_19",4L,"ren","人们在惊愕几秒钟之后，开始热烈地鼓掌，纷纷过来与任是非握手拥抱表示祝贺。",null,null,null,null,null,null,null) );

        blurScriptLst.add(new Script(1L,"Scene_Government_Place_19",1L,"tiper_light","“市政府礼堂，宠物和小孩不得入内”",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(2L,"Scene_Government_Place_19",2L,"tiper_light","“让领导先走！”",null,null,null,null,null,null,null) );

        return new Scene(19L,"Scene_Government_Place_19","市政府礼堂","Government_Place","Scene_Computer_Room_18","Scene_End_20",null,focusScriptLst,blurScriptLst,"“亲，玩游戏的最高境界是在“好人有好报，坏人被法办”的结局中得到教化的升华，本剧本也不例外哈","清木村房产项目投标会在市政府礼堂隆重地举行，结果以是三和集团胜出",null,null,null,null,null,null,false);
    }
    
    private Scene end_20Scene(){
        List<Script> focusScriptLst = new ArrayList<Script>();
        List<Script> blurScriptLst = new ArrayList<Script>();

        focusScriptLst.add(new Script(1L,"Scene_End_20",1L,"tiper_harry","夕阳西下，清木村家家户户生起了炊烟。在阳光的余辉下，一抔新坟冷冷清清的，坟头已零星爬上紫丁草。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(2L,"Scene_End_20",2L,"ren","在坟边的任是非沉默致哀了一会，便掏出手机，说道：“警察同志，我是清木村车祸逃逸案的肇事司机。。。”",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(3L,"Scene_End_20",3L,"liu","他的对手柳河东在投标会上的失败，成了寻常市民茶余饭后的又一个“一元钱”的笑料，据说检察机关已对柳河东提起涉嫌“人身伤害”、“不正当商业交易”和“向公职人员行贿”等诉讼。",null,null,null,null,null,null,null) );
        focusScriptLst.add(new Script(4L,"Scene_End_20",4L,"tiper_harry","Game Over，谢谢！  ",null,null,null,null,null,null,null) );
        
        blurScriptLst.add(new Script(1L,"Scene_End_20",1L,"tiper_light","红雨随心翻作浪，借问瘟君欲何往。青山着意化为桥，纸船明烛照天烧",null,null,null,null,null,null,null) );
        blurScriptLst.add(new Script(2L,"Scene_End_20",2L,"tiper_light","谢谢！",null,null,null,null,null,null,null) );

        return new Scene(20L,"Scene_End_20","清木村","Village","Scene_Government_Place_19","Scene_End_20",null,focusScriptLst,blurScriptLst,"“二月繁霜杀桃花，明年欲嫁今年死。谢谢！ ","尘埃落定，各得其所，Game Over",null,null,null,null,null,null,false);
    }
    
}
