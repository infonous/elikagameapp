package actor.game.ourcause;

import model.game.ourcause.MapInfo;
import screent.game.ourcause.SenceScreent;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;

import elika.game.ourcause.AssetsGame;
import elika.game.ourcause.MainActivity;

public class MapBuilderBtnActor extends Actor {

	public Sprite spriteStationA; 
	public Sprite spriteStationB;
	public Sprite spriteRestaurantA;
	public Sprite spriteRestaurantB;
	public Sprite spriteJianxingA;
	public Sprite spriteJianxingB;
	public Sprite spriteAssisOfficeA;
	public Sprite spriteAssisOfficeB;
	public Sprite spritePainterHomeA;
	public Sprite spritePainterHomeB;
	public Sprite spriteCompanyDormA;
	public Sprite spriteCompanyDormB;
	public Sprite spriteTecherDormA;
	public Sprite spriteTecherDormB;
	public Sprite spritePear12A;
	public Sprite spritePear12B;
	public Sprite spriteChildPlaceA;
	public Sprite spriteChildPlaceB;
	public Sprite spriteVillageA;
	public Sprite spriteVillageB;
	public Sprite spriteDormPlaceA;
	public Sprite spriteDormPlaceB;
	public Sprite spritePoliceStationA;
	public Sprite spritePoliceStationB;
	public Sprite spriteRenBoosHomeA;
	public Sprite spriteRenBoosHomeB;
	public Sprite spriteComputerRoomA;
	public Sprite spriteComputerRoomB;
	public Sprite spriteGovePlaceA;
	public Sprite spriteGovePlaceB;	
	
	public String btnChoice = "";	
	public TextureRegion[]  btnFrames = new TextureRegion[15];
	boolean isTouchView = false;
	
	private static String STATION = "STATION"; //火车站
	private static String RESTAURANT = "RESTAURANT"; //W 快餐店
	private static String JIANXING = "JIANXING"; //嘉兴实业
	private static String ASSISTANT_OFFICE = "ASSISTANT_OFFICE"; // 助理办公室
	private static String PAINTER_HOME = "PAINTER_HOME"; //无二斋画廊
	private static String COMPANY_DORM = "COMPANY_DORM"; //公司宿舍
	private static String TECHER_DORM = "TECHER_DORM"; //教师宿舍
	private static String PEAR12 = "PEAR12"; //十二颗梧桐 X
	private static String CHILDREN_PLACE = "CHILDREN_PLACE"; //儿童福利院 X
	private static String VILLAGE = "VILLAGE"; //清木村 X 
	private static String DORM_PLACE = "DORM_PLACE"; //宿管处
	private static String POLICE_STATION = "POLICE_STATION"; //警察局
	private static String REN_BOOS_HOME = "REN_BOOS_HOME"; //任宅
	private static String COMPUTER_ROOM = "COMPUTER_ROOM"; //电脑室
	private static String GOVERNMENT_PLACE = "GOVERNMENT_PLACE"; //市政府礼堂
	
	public MapBuilderBtnActor() {
		// TODO Auto-generated constructor stub
		super();
	}
	
	public MapBuilderBtnActor(TextureRegion mapTextureRegion) {
		// TODO Auto-generated constructor stub
		super();
		initImageFrames(mapTextureRegion);
		fullSprite();
	}	
	
	public void fullSprite(){
		// TODO Auto-generated constructor stub	
		Vector3 vector = initSpritePoint(STATION);
		spriteStationA = new Sprite(btnFrames[0], 0, 0, 64, 64);
		spriteStationA.setPosition(vector.x, vector.y);
		spriteStationB = new Sprite(btnFrames[0], 0, 64, 64, 64);
		spriteStationB.setPosition(vector.x, vector.y);
		
		vector = initSpritePoint(RESTAURANT);
		spriteRestaurantA = new Sprite(btnFrames[1], 0, 0, 64, 64);
		spriteRestaurantA.setPosition(vector.x, vector.y);
		spriteRestaurantB = new Sprite(btnFrames[1], 0, 64, 64, 64);
		spriteRestaurantB.setPosition(vector.x, vector.y);
		
		vector = initSpritePoint(JIANXING);
		spriteJianxingA = new Sprite(btnFrames[2], 0, 0, 64, 64);
		spriteJianxingA.setPosition(vector.x, vector.y);
		spriteJianxingB = new Sprite(btnFrames[2], 0, 64, 64, 64);
		spriteJianxingB.setPosition(vector.x, vector.y);		
		
		vector = initSpritePoint(ASSISTANT_OFFICE);
		spriteAssisOfficeA = new Sprite(btnFrames[3], 0, 0, 64, 64);
		spriteAssisOfficeA.setPosition(vector.x, vector.y);
		spriteAssisOfficeB = new Sprite(btnFrames[3], 0, 64, 64, 64);
		spriteAssisOfficeB.setPosition(vector.x, vector.y);
		
		vector = initSpritePoint(PAINTER_HOME);
		spritePainterHomeA = new Sprite(btnFrames[4], 0, 0, 64, 64);
		spritePainterHomeA.setPosition(vector.x, vector.y);
		spritePainterHomeB = new Sprite(btnFrames[4], 0, 64, 64, 64);
		spritePainterHomeB.setPosition(vector.x, vector.y);
		
		vector = initSpritePoint(COMPANY_DORM);
		spriteCompanyDormA = new Sprite(btnFrames[5], 0, 0, 64, 64);
		spriteCompanyDormA.setPosition(vector.x, vector.y);
		spriteCompanyDormB = new Sprite(btnFrames[5], 0, 64, 64, 64);
		spriteCompanyDormB.setPosition(vector.x, vector.y);

		vector = initSpritePoint(TECHER_DORM);
		spriteTecherDormA = new Sprite(btnFrames[6], 0, 0, 64, 64);
		spriteTecherDormA.setPosition(vector.x, vector.y);
		spriteTecherDormB = new Sprite(btnFrames[6], 0, 64, 64, 64);
		spriteTecherDormB.setPosition(vector.x, vector.y);
		
		vector = initSpritePoint(PEAR12);
		spritePear12A = new Sprite(btnFrames[7], 0, 0, 64, 64);
		spritePear12A.setPosition(vector.x, vector.y);
		spritePear12B = new Sprite(btnFrames[7], 0, 64, 64, 64);
		spritePear12B.setPosition(vector.x, vector.y);
		
		vector = initSpritePoint(CHILDREN_PLACE);
		spriteChildPlaceA = new Sprite(btnFrames[8], 0, 0, 64, 64);
		spriteChildPlaceA.setPosition(vector.x, vector.y);
		spriteChildPlaceB = new Sprite(btnFrames[8], 0, 64, 64, 64);
		spriteChildPlaceB.setPosition(vector.x, vector.y);
		
		vector = initSpritePoint(VILLAGE);
		spriteVillageA = new Sprite(btnFrames[9], 0, 0, 64, 64);
		spriteVillageA.setPosition(vector.x, vector.y);
		spriteVillageB = new Sprite(btnFrames[9], 0, 64, 64, 64);
		spriteVillageB.setPosition(vector.x, vector.y);
		
		vector = initSpritePoint(DORM_PLACE);
		spriteDormPlaceA = new Sprite(btnFrames[10], 0, 0, 64, 64);
		spriteDormPlaceA.setPosition(vector.x, vector.y);
		spriteDormPlaceB = new Sprite(btnFrames[10], 0, 64, 64, 64);
		spriteDormPlaceB.setPosition(vector.x, vector.y);
		
		vector = initSpritePoint(POLICE_STATION);
		spritePoliceStationA = new Sprite(btnFrames[11], 0, 0, 64, 64);
		spritePoliceStationA.setPosition(vector.x, vector.y);
		spritePoliceStationB = new Sprite(btnFrames[11], 0, 64, 64, 64);
		spritePoliceStationB.setPosition(vector.x, vector.y);
		
		vector = initSpritePoint(REN_BOOS_HOME);
		spriteRenBoosHomeA = new Sprite(btnFrames[12], 0, 0, 64, 64);
		spriteRenBoosHomeA.setPosition(vector.x, vector.y);
		spriteRenBoosHomeB = new Sprite(btnFrames[12], 0, 64, 64, 64);
		spriteRenBoosHomeB.setPosition(vector.x, vector.y);
		
		vector = initSpritePoint(COMPUTER_ROOM);
		spriteComputerRoomA = new Sprite(btnFrames[13], 0, 0, 64, 64);
		spriteComputerRoomA.setPosition(vector.x, vector.y);
		spriteComputerRoomB = new Sprite(btnFrames[13], 0, 64, 64, 64);
		spriteComputerRoomB.setPosition(vector.x, vector.y);
		
		vector = initSpritePoint(GOVERNMENT_PLACE);
		spriteGovePlaceA = new Sprite(btnFrames[14], 0, 0, 64, 64);
		spriteGovePlaceA.setPosition(vector.x, vector.y);
		spriteGovePlaceB = new Sprite(btnFrames[14], 0, 64, 64, 64);
		spriteGovePlaceB.setPosition(vector.x, vector.y);		
	}

	public void initImageFrames(TextureRegion mapTextureRegion){
		TextureRegion[][] tmp = mapTextureRegion.split(64, 128);
		for (int i = 0; i < 15; i++) {
			btnFrames[i] = tmp[0][i];
		}		
	}
	
	//算法：围绕在屏幕中央原点的 200X300 范围散布
	public Vector3 initSpritePoint(String builder){	
		// TODO Auto-generated constructor stub	
		Vector3 vector = new Vector3();
		
		final float baseX = AssetsGame._SreentWidth * 0.5f;
		final float baseY = AssetsGame._ScreentHeight* 0.5f;
		final float rateX = (baseX > 512f ? 512f : 256f);
		final float rateY = (baseY > 256f ? 256f : 128f);

		float _x = (baseX + rateX - 10f);
		float _y = (baseY + rateY - 10f);
		//右上角
		if(builder == "STATION"){
			vector.x = _x - 128f;
			vector.y = _y - 64f;
		}
		if( builder == "RESTAURANT"){			
			vector.x = _x - 64f;
			vector.y = _y - 64f;
		}
		
		//左上角
		_x = (baseX - rateX - 30f);
		_y = (baseY + rateY - 30f);
		if( builder == "JIANXING"){			
			vector.x = _x + 128f;
			vector.y = _y - 32f;
		}	
		if( builder == "ASSISTANT_OFFICE"){
			vector.x = _x + 64f;
			vector.y = _y - 64f;
		}
		if( builder == "PAINTER_HOME"){
			vector.x = _x + 128f;
			vector.y = _y - 128f;
		}
		if( builder == "COMPANY_DORM"){
			vector.x = _x + 64f;
			vector.y = _y - 128f;	
		}
		
		
		//左下角
		_x = (baseX - rateX + 64f);
		_y = (baseY - rateY);
		if( builder == "TECHER_DORM"){
			vector.x = _x;
			vector.y = _y;			
		}
		if( builder == "PEAR12"){
			vector.x = _x + 64f;
			vector.y = _y + 64f;
		}
		if( builder == "DORM_PLACE"){
			vector.x = _x + 64f;
			vector.y = _y + 128f;
		}
		
		
		//屏幕中心
		_x = baseX;
		_y = baseY;
		if( builder == "CHILDREN_PLACE"){
			vector.x = _x + 64f;
			vector.y = _y;	
		}

		if( builder == "POLICE_STATION"){
			vector.x = _x - 64f;
			vector.y = _y;	
		}
		
		if( builder == "GOVERNMENT_PLACE"){
			vector.x = _x;
			vector.y = baseY - 64f;	
		}
		
		//右下角	
		_x = (baseX + rateX);
		_y = (baseY - rateY);
		if( builder == "VILLAGE"){
			vector.x = _x - 128f;
			vector.y = _y;			
		}

		if( builder == "REN_BOOS_HOME"){
			vector.x = _x - 64f;
			vector.y = _y + 64f;			
		}
		if( builder == "COMPUTER_ROOM"){
			vector.x = _x - 128f;
			vector.y = _y + 64f;
		}
		
		return vector;
	}
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		// TODO Auto-generated method stub		
		if((btnChoice == STATION) && (isTouchView==true)) spriteStationB.draw(batch); else spriteStationA.draw(batch);
		if((btnChoice == RESTAURANT) && (isTouchView==true)) spriteRestaurantB.draw(batch); else spriteRestaurantA.draw(batch);
		if((btnChoice == JIANXING) && (isTouchView==true)) spriteJianxingB.draw(batch); else spriteJianxingA.draw(batch);
		if((btnChoice == ASSISTANT_OFFICE) && (isTouchView==true)) spriteAssisOfficeB.draw(batch); else spriteAssisOfficeA.draw(batch);
		if((btnChoice == PAINTER_HOME) && (isTouchView==true)) spritePainterHomeB.draw(batch); else spritePainterHomeA.draw(batch);
		if((btnChoice == COMPANY_DORM) && (isTouchView==true)) spriteCompanyDormB.draw(batch); else spriteCompanyDormA.draw(batch);
		if((btnChoice == TECHER_DORM) && (isTouchView==true)) spriteTecherDormB.draw(batch); else spriteTecherDormA.draw(batch);
		if((btnChoice == PEAR12) && (isTouchView==true)) spritePear12B.draw(batch); else spritePear12A.draw(batch);
		if((btnChoice == CHILDREN_PLACE) && (isTouchView==true)) spriteChildPlaceB.draw(batch); else spriteChildPlaceA.draw(batch);
		if((btnChoice == VILLAGE) && (isTouchView==true)) spriteVillageB.draw(batch); else spriteVillageA.draw(batch);
		if((btnChoice == DORM_PLACE) && (isTouchView==true)) spriteDormPlaceB.draw(batch); else spriteDormPlaceA.draw(batch);
		if((btnChoice == POLICE_STATION) && (isTouchView==true)) spritePoliceStationB.draw(batch); else spritePoliceStationA.draw(batch);
		if((btnChoice == REN_BOOS_HOME) && (isTouchView==true)) spriteRenBoosHomeB.draw(batch); else spriteRenBoosHomeA.draw(batch);
		if((btnChoice == COMPUTER_ROOM) && (isTouchView==true)) spriteComputerRoomB.draw(batch); else spriteComputerRoomA.draw(batch);
		if((btnChoice == GOVERNMENT_PLACE) && (isTouchView==true)) spriteGovePlaceB.draw(batch); else spriteGovePlaceA.draw(batch);
		
		touchDoneIt();
		
	}
	
	public void touchDoneIt(){
		if((btnChoice == STATION) && (isTouchView==true)) MainActivity.game.setScreen(new SenceScreent(new MapInfo("Station", "火车站"), null));
		if((btnChoice == RESTAURANT) && (isTouchView==true)) MainActivity.game.setScreen(new SenceScreent(new MapInfo("Restaurant", "W快餐店"), null));
		if((btnChoice == JIANXING) && (isTouchView==true)) MainActivity.game.setScreen(new SenceScreent(new MapInfo("Jianxing","嘉兴实业"), null));
		if((btnChoice == ASSISTANT_OFFICE) && (isTouchView==true)) MainActivity.game.setScreen(new SenceScreent(new MapInfo("Assistant_Office","助理办公室"), null));
		if((btnChoice == PAINTER_HOME) && (isTouchView==true)) MainActivity.game.setScreen(new SenceScreent(new MapInfo("Painter_Home","无二斋画廊"), null));
		if((btnChoice == COMPANY_DORM) && (isTouchView==true)) MainActivity.game.setScreen(new SenceScreent(new MapInfo("Company_Dorm","公司宿舍"), null));
		if((btnChoice == TECHER_DORM) && (isTouchView==true)) MainActivity.game.setScreen(new SenceScreent(new MapInfo("Techer_Dorm","教师宿舍"), null));
		if((btnChoice == PEAR12) && (isTouchView==true)) MainActivity.game.setScreen(new SenceScreent(new MapInfo("Pear12","十二颗梧桐"), null));
		if((btnChoice == CHILDREN_PLACE) && (isTouchView==true)) MainActivity.game.setScreen(new SenceScreent(new MapInfo("Children_Place","儿童福利院"), null));
		if((btnChoice == VILLAGE) && (isTouchView==true)) MainActivity.game.setScreen(new SenceScreent(new MapInfo("Village","清木村"), null));
		if((btnChoice == DORM_PLACE) && (isTouchView==true)) MainActivity.game.setScreen(new SenceScreent(new MapInfo("Dorm_Place","宿管处"), null));
		if((btnChoice == POLICE_STATION) && (isTouchView==true)) MainActivity.game.setScreen(new SenceScreent(new MapInfo("Police_Station","警察局"), null));
		if((btnChoice == REN_BOOS_HOME) && (isTouchView==true)) MainActivity.game.setScreen(new SenceScreent(new MapInfo("Ren_Boos_Home","任宅"), null));
		if((btnChoice == COMPUTER_ROOM) && (isTouchView==true)) MainActivity.game.setScreen(new SenceScreent(new MapInfo("Computer_Room","电脑室"), null));
		if((btnChoice == GOVERNMENT_PLACE) && (isTouchView==true)) MainActivity.game.setScreen(new SenceScreent(new MapInfo("Government_Place","市政府礼堂"), null));	

	}
	
	
	@Override
	public Actor hit(float x, float y, boolean touchable){		
		// TODO Auto-generated method stub
        if (x > 0 && y > 0 
        		&& (  ((spriteStationA.getY() + spriteStationA.getHeight()) >= y ) && (spriteStationA.getY() <= y) )
        		&& (  ((spriteStationA.getX() + spriteStationA.getWidth()) >= x  ) && (spriteStationA.getX() <= x) )
        		) {             	
        	btnChoice = STATION;
        	isTouchView = true;
        	return this;
        }else if (x > 0 && y > 0 
        		&& (  ((spriteRestaurantA.getY() + spriteRestaurantA.getHeight()) >= y ) && (spriteRestaurantA.getY() <= y) )
        		&& (  ((spriteRestaurantA.getX() + spriteRestaurantA.getWidth()) >= x  ) && (spriteRestaurantA.getX() <= x) )
        		) {             	
        	btnChoice = RESTAURANT;
        	isTouchView = true;
        	return this;
        }else if (x > 0 && y > 0 
        		&& (  ((spriteJianxingA.getY() + spriteJianxingA.getHeight()) >= y ) && (spriteJianxingA.getY() <= y) )
        		&& (  ((spriteJianxingA.getX() + spriteJianxingA.getWidth()) >= x  ) && (spriteJianxingA.getX() <= x) )
        		) {             	
        	btnChoice = JIANXING;
        	isTouchView = true;
        	return this;
        }else if (x > 0 && y > 0 
        		&& (  ((spriteAssisOfficeA.getY() + spriteAssisOfficeA.getHeight()) >= y ) && (spriteAssisOfficeA.getY() <= y) )
        		&& (  ((spriteAssisOfficeA.getX() + spriteAssisOfficeA.getWidth()) >= x  ) && (spriteAssisOfficeA.getX() <= x) )
        		) {             	
        	btnChoice = ASSISTANT_OFFICE;
        	isTouchView = true;
        	return this;
        }else if (x > 0 && y > 0 
        		&& (  ((spritePainterHomeA.getY() + spritePainterHomeA.getHeight()) >= y ) && (spritePainterHomeA.getY() <= y) )
        		&& (  ((spritePainterHomeA.getX() + spritePainterHomeA.getWidth()) >= x  ) && (spritePainterHomeA.getX() <= x) )
        		) {             	
        	btnChoice = PAINTER_HOME;
        	isTouchView = true;
        	return this;
        }else if (x > 0 && y > 0 
        		&& (  ((spriteCompanyDormA.getY() + spriteCompanyDormA.getHeight()) >= y ) && (spriteCompanyDormA.getY() <= y) )
        		&& (  ((spriteCompanyDormA.getX() + spriteCompanyDormA.getWidth()) >= x  ) && (spriteCompanyDormA.getX() <= x) )
        		) {             	
        	btnChoice = COMPANY_DORM;
        	isTouchView = true;
        	return this;
        }else if (x > 0 && y > 0 
        		&& (  ((spriteTecherDormA.getY() + spriteTecherDormA.getHeight()) >= y ) && (spriteTecherDormA.getY() <= y) )
        		&& (  ((spriteTecherDormA.getX() + spriteTecherDormA.getWidth()) >= x  ) && (spriteTecherDormA.getX() <= x) )
        		) {             	
        	btnChoice = TECHER_DORM;
        	isTouchView = true;
        	return this;
        }else if (x > 0 && y > 0 
        		&& (  ((spritePear12A.getY() + spritePear12A.getHeight()) >= y ) && (spritePear12A.getY() <= y) )
        		&& (  ((spritePear12A.getX() + spritePear12A.getWidth()) >= x  ) && (spritePear12A.getX() <= x) )
        		) {             	
        	btnChoice = PEAR12;
        	isTouchView = true;
        	return this;
        }else if (x > 0 && y > 0 
        		&& (  ((spriteChildPlaceA.getY() + spriteChildPlaceA.getHeight()) >= y ) && (spriteChildPlaceA.getY() <= y) )
        		&& (  ((spriteChildPlaceA.getX() + spriteChildPlaceA.getWidth()) >= x  ) && (spriteChildPlaceA.getX() <= x) )
        		) {             	
        	btnChoice = CHILDREN_PLACE;
        	isTouchView = true;
        	return this;
        }else if (x > 0 && y > 0 
        		&& (  ((spriteVillageA.getY() + spriteVillageA.getHeight()) >= y ) && (spriteVillageA.getY() <= y) )
        		&& (  ((spriteVillageA.getX() + spriteVillageA.getWidth()) >= x  ) && (spriteVillageA.getX() <= x) )
        		) {             	
        	btnChoice = VILLAGE;
        	isTouchView = true;
        	return this;
        }else if (x > 0 && y > 0 
        		&& (  ((spriteDormPlaceA.getY() + spriteDormPlaceA.getHeight()) >= y ) && (spriteDormPlaceA.getY() <= y) )
        		&& (  ((spriteDormPlaceA.getX() + spriteDormPlaceA.getWidth()) >= x  ) && (spriteDormPlaceA.getX() <= x) )
        		) {             	
        	btnChoice = DORM_PLACE;
        	isTouchView = true;
        	return this;
        }else if (x > 0 && y > 0 
        		&& (  ((spritePoliceStationA.getY() + spritePoliceStationA.getHeight()) >= y ) && (spritePoliceStationA.getY() <= y) )
        		&& (  ((spritePoliceStationA.getX() + spritePoliceStationA.getWidth()) >= x  ) && (spritePoliceStationA.getX() <= x) )
        		) {             	
        	btnChoice = POLICE_STATION;
        	isTouchView = true;
        	return this;
        }else if (x > 0 && y > 0 
        		&& (  ((spriteRenBoosHomeA.getY() + spriteRenBoosHomeA.getHeight()) >= y ) && (spriteRenBoosHomeA.getY() <= y) )
        		&& (  ((spriteRenBoosHomeA.getX() + spriteRenBoosHomeA.getWidth()) >= x  ) && (spriteRenBoosHomeA.getX() <= x) )
        		) {             	
        	btnChoice = REN_BOOS_HOME;
        	isTouchView = true;
        	return this;
        }else if (x > 0 && y > 0 
        		&& (  ((spriteComputerRoomA.getY() + spriteComputerRoomA.getHeight()) >= y ) && (spriteComputerRoomA.getY() <= y) )
        		&& (  ((spriteComputerRoomA.getX() + spriteComputerRoomA.getWidth()) >= x  ) && (spriteComputerRoomA.getX() <= x) )
        		) {             	
        	btnChoice = COMPUTER_ROOM;
        	isTouchView = true;
        	return this;
        }else if (x > 0 && y > 0 
        		&& (  ((spriteGovePlaceA.getY() + spriteGovePlaceA.getHeight()) >= y ) && (spriteGovePlaceA.getY() <= y) )
        		&& (  ((spriteGovePlaceA.getX() + spriteGovePlaceA.getWidth()) >= x  ) && (spriteGovePlaceA.getX() <= x) )
        		) {             	
        	btnChoice = GOVERNMENT_PLACE;
        	isTouchView = true;
        	return this;
        }else {
        	btnChoice = "";
        	isTouchView = false;
        	return null;      	
        }
	}
}
