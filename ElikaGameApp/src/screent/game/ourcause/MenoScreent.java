package screent.game.ourcause;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import elika.game.ourcause.AssetsGame;
import elika.game.ourcause.MainActivity;

public class MenoScreent implements Screen, GestureListener {

	private Stage stage;	
	private Texture texture;
	
	private SpriteBatch batch;
	private BitmapFont scriptFont;
	private String arrayList[] = AssetsGame.strMeno.split(";"); //自动生成数组：浏览历史内容
	private int listPageCount = 1; //总页数
	private int pageNumber = 0; //当前页码
	//private int listTotal = 20; //总记录条数
	private int listShip = 5; //每页显示的记录条数
	
	private ImageButton btnMap;
	private ImageButton btnLibary;
	private ImageButton btnHelp;
	private ImageButton btnReturn;	
	
	public MenoScreent() {
		// TODO Auto-generated constructor stub
		super();
		if (stage == null) {
            stage = new Stage(AssetsGame._SreentWidth, AssetsGame._ScreentHeight, true);
            texture = new Texture(Gdx.files.internal("MenoUI.png"));
            
            scriptFont = new BitmapFont(Gdx.files.internal("sugarMeno.fnt"), Gdx.files.internal("sugarMeno.png"), false);
            batch = new SpriteBatch();            
            listPageCount = (int)(((arrayList.length)/listShip) + 1); //至少有一页：五条内容为一页
            
            //填充背景图片
            Image imageBcakGroup = new Image(new TextureRegion(texture, 0, 248, 8, 8));
            imageBcakGroup.setFillParent(true);
            stage.addActor(imageBcakGroup);
            
            //填充背景图片：标题
            Image imageTitle = new Image(new TextureRegion(texture, 0, 200, 240, 50));
            imageTitle.setPosition(AssetsGame._SreentSpaceX, (AssetsGame._ScreentHeight - AssetsGame._SreentSpaceY - 50f));
            stage.addActor(imageTitle);
            
            //填充人物图片：左下角
            Image imagePeople = new Image(new TextureRegion(texture, 0, 256, 256, 256));
            imagePeople.setPosition(AssetsGame._SreentSpaceX, AssetsGame._SreentSpaceY); 
            stage.addActor(imagePeople);
            
            doneCreateBack();
            doneCreateButtones();
		}
		
	}
	
	//建立主体内容：画出两块方形，制造出阴影效果，再添加文字内容
	public void doneCreateBack(){
		
		//阴影部分
        Image imageDownBand = new Image(new TextureRegion(texture, 240, 244, 16, 12 )); 
        imageDownBand.setPosition( (AssetsGame._SreentWidth - 512f) * 0.5f + 10f, (AssetsGame._ScreentHeight - 384f) * 0.65f - 10f ); 
        imageDownBand.setScale(32f);
        stage.addActor(imageDownBand);
        
		//面板部分
        Image imageTopBand = new Image(new TextureRegion(texture, 240, 232, 16, 12 )); 
        imageTopBand.setPosition( (AssetsGame._SreentWidth - 512f) * 0.5f, (AssetsGame._ScreentHeight - 384f) * 0.65f ); 
        imageTopBand.setScale(32f);
        stage.addActor(imageTopBand);  
	}
	
	
	//创建各按钮，以及对应的监听事件
	public void doneCreateButtones(){
		
        float btnX = AssetsGame._SreentWidth - AssetsGame._SreentSpaceX - 128f;
        float btnY = AssetsGame._ScreentHeight * 0.35f;
        //按钮：地图、 图书馆、游戏帮助、退出
        btnMap = new ImageButton(new TextureRegionDrawable(new TextureRegion(texture, 0, 0, 128, 50)), 
        		new TextureRegionDrawable(new TextureRegion(texture, 128, 0, 128, 50)));
        btnMap.setPosition(btnX, btnY + 150f);
        btnMap.addListener(new InputListener(){            	
            @Override
            public void touchUp(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub              	
                super.touchUp(event, x, y, pointer, button);
                MainActivity.game.setScreen(new MapPageScreent());
            }
            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub
                return true;
            }            
        });
        
        btnLibary = new ImageButton(new TextureRegionDrawable(new TextureRegion(texture, 0, 50, 128, 50)), 
        		new TextureRegionDrawable(new TextureRegion(texture, 128, 50, 128, 50)));
        btnLibary.setPosition(btnX, btnY + 100f);
        btnLibary.addListener(new InputListener(){            	
            @Override
            public void touchUp(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub              	
                super.touchUp(event, x, y, pointer, button);
                MainActivity.game.setScreen(new LibraryScreent());
            }
            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub
                return true;
            }                
        });
        
        btnHelp = new ImageButton(new TextureRegionDrawable(new TextureRegion(texture, 0, 100, 128, 50)), 
        		new TextureRegionDrawable(new TextureRegion(texture, 128, 100, 128, 50)));
        btnHelp.setPosition(btnX, btnY + 50f);
        btnHelp.addListener(new InputListener(){            	
            @Override
            public void touchUp(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub              	
                super.touchUp(event, x, y, pointer, button);
                MainActivity.game.setScreen(new HelpScreent());
            }
            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub
                return true;
            }                
        });
        
        btnReturn = new ImageButton(new TextureRegionDrawable(new TextureRegion(texture, 0, 150, 128, 50)), 
        		new TextureRegionDrawable(new TextureRegion(texture, 128, 150, 128, 50)));
        btnReturn.setPosition(btnX, btnY);
        btnReturn.addListener(new InputListener(){            	
            @Override
            public void touchUp(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub              	
                super.touchUp(event, x, y, pointer, button);
                MainActivity.game.setScreen(new MainPageScreent());
            }
            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub
                return true;
            }                
        });
        
        InputMultiplexer multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(stage);
        multiplexer.addProcessor(new GestureDetector(this));
        Gdx.input.setInputProcessor(multiplexer);
        
        stage.addActor(btnReturn);
        stage.addActor(btnHelp);
        stage.addActor(btnLibary);
        stage.addActor(btnMap); 
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		texture.dispose();
        stage.dispose(); 
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	//获取备注内容 arrayList[]
	public String getContentByFling(){
		String txt = "";
		for(int i=(pageNumber*listShip); i<arrayList.length; i++ ){
			if(i < ((pageNumber+1)*listShip)){
				txt += "●" + arrayList[i] + ";\n\n"; //String.valueOf(i + 1) + "."
			}else{
				break;
			}
		}		
		return txt;
	}
	
	@Override
	public void render(float arg0) {
		// TODO Auto-generated method stub
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw(); 
        
        //填充文本内容
        String textMsg = getContentByFling();
        batch.begin();
        scriptFont.drawWrapped(batch, textMsg, (AssetsGame._SreentWidth - 512f)*0.5f + 32f, (AssetsGame._ScreentHeight - 384f) * 0.65f + 352f, 448f );
        batch.end();
	}

	@Override
	public void resize(int arg0, int arg1) {
		// TODO Auto-generated method stub
		AssetsGame.doneScreentChange();
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean fling(float arg0, float arg1, int arg2) {
		// TODO Auto-generated method stub
		//由下向上
		if( ( arg0 > 0f ) && (arg1 < 0f ) ) {
			pageNumber++;
			if(pageNumber >= listPageCount){			
				pageNumber = 0;
			}
		}		
		//由上向下
		if( ( arg0 < 0f ) && (arg1 > 0f ) ) {
			pageNumber--;
			if(pageNumber <= -1){			
				pageNumber = listPageCount - 1;
			}
		}
		
		return true;
	}

	@Override
	public boolean longPress(float arg0, float arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pan(float arg0, float arg1, float arg2, float arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pinch(Vector2 arg0, Vector2 arg1, Vector2 arg2, Vector2 arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean tap(float arg0, float arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(float arg0, float arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean zoom(float arg0, float arg1) {
		// TODO Auto-generated method stub
		return false;
	}

}
