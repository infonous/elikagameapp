package screent.game.ourcause;

import model.game.ourcause.MapInfo;
import model.game.ourcause.Question;
import model.game.ourcause.SceneEntity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.ui.Window.WindowStyle;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import elika.game.ourcause.AssetsGame;
import elika.game.ourcause.MainActivity;

enum enumPeopleImage {  
	wonglei, lizi, police, guangxianyuan, ano, mather, ren, liu, dean, storer, boss, tiper_light, tiper_harry;
}

public class SenceScreent implements Screen, GestureListener {

	private Stage stage;	
	private Texture baseTexture; //人物 + 菜单 图片
	private Texture SenceTexture; //场景图片
	
	private TextureRegion[]  peopleFrames = new TextureRegion[14];
	
	private ImageButton btnMap;
	private ImageButton btnLibary;
	private ImageButton btnHistory;
	private ImageButton btnHelp;
	private ImageButton btnReturn;		
	
	private Window tipsDialogWindow = null;
	private BitmapFont scriptFont;
	private boolean isExistTipsDialogWindow = false;
	private Label labTips = null;
	private String strTips = "", winTipsTitle ="", winQuestionTitle ="";//提示
	
	private int scriptCount = 0;
	private SpriteBatch batch;
	
	private ImageButton btnTips; //场景提示
	private ImageButton btnSkip; //场景跳转
	private ImageButton btnQuestion; //场景难题
	private boolean isBlur = false; //判断当前场景是否符合访问条件的，用于控制  物品和解迷 两个按钮的显示
	
	private String skipSceneCode = "";
	private boolean isHaveQuestion = false;
	
	private Window questionDialogWindow = null; //问题窗口
	private Question question = null; //问题
	
	
	public SenceScreent() {
		// TODO Auto-generated constructor stub
		super();
	}

	//方法重构：以地图点击的方式进入; 以场景跳转的方式进入
	public SenceScreent(MapInfo item, String skipSceneCodePara) {
		// TODO Auto-generated constructor stub		
		super();
		
		SceneEntity entity = new SceneEntity();
		entity.setSceneStore(AssetsGame.sceneStore);		
		entity.setHistory(AssetsGame.history);		
		
		//以地图点击的方式进入;
		if(skipSceneCodePara == null){
			AssetsGame.sceneItem = entity.getCurrentScene(item.mapCode);
		}
		//以场景跳转的方式进入
		if(item == null){
			AssetsGame.sceneItem = entity.getSkipToScene(skipSceneCodePara, 0L);
		}
		
		AssetsGame.history = entity.getHistory();
		if(entity.getMeno().length() > 0){ //区别于 entity.getHistory(), 这里不需判断是否已经访问过场景；后访问的内容排在最前面
			AssetsGame.strMeno = entity.getMeno() + ( AssetsGame.strMeno.length() > 0 ? ";" : "") + AssetsGame.strMeno;
		}
		AssetsGame.currentSence = entity.getCurrentSence();
		AssetsGame.scriptIndex = 0;
		
		//切换 已访问和未访问的脚本
		if(AssetsGame.sceneItem.isFocus()){			
			AssetsGame.scriptList = AssetsGame.sceneItem.getFocusScriptLst();
			isBlur = true;
			
			//如为 非解迷场景，则直接保存游戏进程；如果 解迷成功，则另外调用保存
			if(AssetsGame.sceneItem.getQuestion() == null){	
				AssetsGame.writeRecordData(); //保存游戏进度
			}
		}else{			
			AssetsGame.scriptList = AssetsGame.sceneItem.getBlurScriptrLst();
		}
		
		scriptCount = AssetsGame.scriptList.size(); //取脚本数量, 用于：fling()
		strTips = AssetsGame.sceneItem.getTips();
		skipSceneCode = (AssetsGame.sceneItem.getSkipSceneCode() == null ? "": AssetsGame.sceneItem.getSkipSceneCode());
		isHaveQuestion = (AssetsGame.sceneItem.getQuestion() == null ? false: true);
		question = AssetsGame.sceneItem.getQuestion();
		
		if (stage == null) {	
			
            stage = new Stage(AssetsGame._SreentWidth, AssetsGame._ScreentHeight, true);
            baseTexture = new Texture(Gdx.files.internal("senceUI.png")); 
            SenceTexture = new Texture(Gdx.files.internal(doneGetSenceImageFilePath( (AssetsGame.sceneItem.getPkey()).intValue() )));            
            scriptFont = new BitmapFont(Gdx.files.internal("sugarScript.fnt"), Gdx.files.internal("sugarScript.png"), false);
            
            batch = new SpriteBatch();
            
            //人物图标切割
            doneCreatePeopleImage(baseTexture);
            //生成提示窗口
            doneCreateTipsWindow();            
            //生成各背景图片
            doneCreateBackgroupImage();            
            //生成屏幕按钮，并监听按钮事件
            doneCreateButton();
		}
	}
	
	//生成屏幕各背景图片
	public void doneCreateBackgroupImage(){
        //填充背景
        Image imageBcakGroup = new Image(new TextureRegion(baseTexture, 1016, 504, 8, 8 ));
        imageBcakGroup.setFillParent(true);
        stage.addActor(imageBcakGroup);
        
        //填充场景背景图片
        Image imageSenceBk = new Image(SenceTexture);
        imageSenceBk.setPosition((AssetsGame._SreentWidth - 1024f)*0.5f, (AssetsGame._ScreentHeight - 512f)*0.65f);
        stage.addActor(imageSenceBk);
        
        //填充对话框
        Image imagePanel = new Image(new TextureRegion(baseTexture, 512, 256, 512, 200 ));
        imagePanel.setPosition( (AssetsGame._SreentWidth - 512f)*0.5f, AssetsGame._SreentSpaceY);            
        stage.addActor(imagePanel);         
	}
	
	//生成屏幕各按钮
	public void doneCreateButton(){
		
        float btnX = AssetsGame._SreentWidth - AssetsGame._SreentSpaceX - 128f;
        float btnY = AssetsGame._ScreentHeight * 0.35f;
        //按钮：地图、 图书馆、备忘本、游戏帮助、新再玩、退出
        btnMap = new ImageButton(new TextureRegionDrawable(new TextureRegion(baseTexture, 0, 256, 128, 50)), 
        		new TextureRegionDrawable(new TextureRegion(baseTexture, 128, 256, 128, 50)));
        btnMap.setPosition(btnX, btnY + 200f);
        btnMap.addListener(new InputListener(){            	
            @Override
            public void touchUp(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub              	
                super.touchUp(event, x, y, pointer, button);
                MainActivity.game.setScreen(new MapPageScreent());
            }
            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub
                return true;
            }                
        });
        
        btnLibary = new ImageButton(new TextureRegionDrawable(new TextureRegion(baseTexture, 0, 306, 128, 50)), 
        		new TextureRegionDrawable(new TextureRegion(baseTexture, 128, 306, 128, 50)));
        btnLibary.setPosition(btnX, btnY + 150f);
        btnLibary.addListener(new InputListener(){            	
            @Override
            public void touchUp(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub              	
                super.touchUp(event, x, y, pointer, button);
                MainActivity.game.setScreen(new LibraryScreent());
            }
            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub
                return true;
            }                
        });
        
        btnHistory = new ImageButton(new TextureRegionDrawable(new TextureRegion(baseTexture, 0, 356, 128, 50)), 
        		new TextureRegionDrawable(new TextureRegion(baseTexture, 128, 356, 128, 50)));
        btnHistory.setPosition(btnX, btnY + 100f);
        btnHistory.addListener(new InputListener(){            	
            @Override
            public void touchUp(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub              	
                super.touchUp(event, x, y, pointer, button);
                MainActivity.game.setScreen(new MenoScreent());
            } 
            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub
                return true;
            } 
        });
        
        btnHelp = new ImageButton(new TextureRegionDrawable(new TextureRegion(baseTexture, 0, 406, 128, 50)), 
        		new TextureRegionDrawable(new TextureRegion(baseTexture, 128, 406, 128, 50)));
        btnHelp.setPosition(btnX, btnY + 50f);
        btnHelp.addListener(new InputListener(){            	
            @Override
            public void touchUp(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub              	
                super.touchUp(event, x, y, pointer, button);
                MainActivity.game.setScreen(new HelpScreent());
            }
            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub
                return true;
            }                
        });
        
        btnReturn = new ImageButton(new TextureRegionDrawable(new TextureRegion(baseTexture, 0, 456, 128, 50)), 
        		new TextureRegionDrawable(new TextureRegion(baseTexture, 128, 456, 128, 50)));
        btnReturn.setPosition(btnX, btnY);
        btnReturn.addListener(new InputListener(){            	
            @Override
            public void touchUp(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub              	
                super.touchUp(event, x, y, pointer, button);
                MainActivity.game.setScreen(new MainPageScreent());
            }
            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub
                return true;
            }                
        });
        
        //提示按钮：弹性开关
        btnTips = new ImageButton(new TextureRegionDrawable(new TextureRegion(baseTexture, 256, 256, 64, 50)), 
        		new TextureRegionDrawable(new TextureRegion(baseTexture, 320, 256, 64, 50)));
        btnTips.setPosition((AssetsGame._SreentWidth*0.5f + 192f), AssetsGame._SreentSpaceY + 200f); 
        btnTips.addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub              	
                super.touchUp(event, x, y, pointer, button);
            }
        	@Override
        	public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {            		
        		if(isExistTipsDialogWindow == false){            			
        			isExistTipsDialogWindow = true;
        			stage.addActor(tipsDialogWindow);
        			
        	        //提示的内容资料
        	        labTips = new Label(strTips, new LabelStyle(scriptFont, Color.WHITE));        	        
        	        labTips.setPosition(48f, 48f);
        	        labTips.setWidth(416f);
        	        labTips.setWrap(true);
        	        tipsDialogWindow.addActor(labTips);
        		}else{            			
        			isExistTipsDialogWindow = false;
        			labTips.remove();
        			tipsDialogWindow.remove();
        		}     
        		
        		return true;
        	}
        	
        });
        
        //附加的场景跳转和问题按钮
        btnSkip = new ImageButton(new TextureRegionDrawable(new TextureRegion(baseTexture, 256, 306, 64, 50)), 
        		new TextureRegionDrawable(new TextureRegion(baseTexture, 320, 306, 64, 50)));
        btnSkip.setPosition((AssetsGame._SreentWidth*0.5f + 128f), AssetsGame._SreentSpaceY + 200f);
        btnSkip.addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub              	
                super.touchUp(event, x, y, pointer, button);
            }
        	@Override
        	public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {            		
        		MainActivity.game.setScreen(new SenceScreent(null, skipSceneCode));
        		return true;
        	}     	
        });
        
        btnQuestion = new ImageButton(new TextureRegionDrawable(new TextureRegion(baseTexture, 256, 356, 64, 50)), 
        		new TextureRegionDrawable(new TextureRegion(baseTexture, 320, 356, 64, 50)));
        btnQuestion.setPosition((AssetsGame._SreentWidth*0.5f + 64f), AssetsGame._SreentSpaceY + 200f);
        btnQuestion.addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub              	
                super.touchUp(event, x, y, pointer, button);
            }
        	@Override
        	public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {            		
        		doneCreateQuestionWindow();
        		return true;
        	}
        });        
        
        InputMultiplexer multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(stage);
        multiplexer.addProcessor(new GestureDetector(this));
        Gdx.input.setInputProcessor(multiplexer);
    	
        //利用场景标识，判断是否加入跳转或问题的按钮
    	if( (isBlur == true) && (skipSceneCode.length() > 0) ) stage.addActor(btnSkip);
    	if( (isBlur == true) && (isHaveQuestion==true)) stage.addActor(btnQuestion);
    	if (isBlur == true) stage.addActor(btnTips);
    	
    	System.out.println( (isBlur == true ? "T" : "F") + " " + (skipSceneCode.length() > 0? skipSceneCode : "XXX"));

        stage.addActor(btnReturn);
        stage.addActor(btnHelp);
        stage.addActor(btnHistory);
        stage.addActor(btnLibary);
        stage.addActor(btnMap); 
	}

	//生成提示窗口
	public void doneCreateTipsWindow() {
		
		TextureRegionDrawable windowBackgroup = new TextureRegionDrawable(new TextureRegion(baseTexture, 512, 128, 512, 128 ));		
		WindowStyle style = new WindowStyle(scriptFont, Color.WHITE, windowBackgroup);
		
		tipsDialogWindow = new Window(winTipsTitle, style);
		tipsDialogWindow.setWidth(512f);
		tipsDialogWindow.setHeight(128f);
		tipsDialogWindow.setPosition( AssetsGame._SreentSpaceX, (AssetsGame._ScreentHeight - AssetsGame._SreentSpaceY - 128f));
		tipsDialogWindow.setMovable(true);
	}
	
	//生成问题窗口
	public void doneCreateQuestionWindow() {
		
		TextureRegionDrawable windowBackgroup = new TextureRegionDrawable(new TextureRegion(baseTexture, 1008, 504, 8, 8 ));		
		WindowStyle style = new WindowStyle(scriptFont, Color.WHITE, windowBackgroup);
		
		questionDialogWindow = new Window(winQuestionTitle, style);
		questionDialogWindow.setWidth(AssetsGame._SreentWidth);
		questionDialogWindow.setHeight(AssetsGame._ScreentHeight);
		questionDialogWindow.setPosition(0,0);
		questionDialogWindow.setMovable(true);		
  
        Image imageQuestionBand = new Image(new TextureRegion(baseTexture, 390, 460, 16, 8 )); 
        imageQuestionBand.setPosition( (AssetsGame._SreentWidth - 512f) * 0.5f, (AssetsGame._ScreentHeight - 256f) * 0.72f ); 
        imageQuestionBand.setScale(32f); //512,256
        questionDialogWindow.addActor(imageQuestionBand);
        
        //question
        Label labQuestion = new Label(question.getQuestionContent().toString(), new LabelStyle(scriptFont, Color.WHITE));        	        
        labQuestion.setPosition((AssetsGame._SreentWidth - 384f) * 0.5f + 32f, (AssetsGame._ScreentHeight - 192f) * 0.72f + 128f);
        labQuestion.setWidth(320f);
        labQuestion.setWrap(true);
        questionDialogWindow.addActor(labQuestion);
        
        TextField.TextFieldStyle tfstyle = new TextField.TextFieldStyle(); 
        tfstyle.font = new BitmapFont();
        tfstyle.fontColor = Color.BLACK;
        tfstyle.background = new SpriteDrawable(new Sprite( new TextureRegion(baseTexture, 896, 456, 128, 32 ) ));
        tfstyle.selection = new SpriteDrawable(new Sprite( new TextureRegion(baseTexture, 640, 128, 2, 32 ) ));
        tfstyle.cursor = new SpriteDrawable(new Sprite( new TextureRegion(baseTexture, 640, 256, 2, 32 ) ));
        final TextField equalField = new TextField("",tfstyle); 
        equalField.setPosition((AssetsGame._SreentWidth - 384f) * 0.5f + 32f, (AssetsGame._ScreentHeight - 192f) * 0.72f + 64f);
        equalField.setMaxLength(60);
        equalField.setMessageText("Please enter here!");
        questionDialogWindow.addActor(equalField);
        
        //提交
        ImageButton btnSumbit = new ImageButton(new TextureRegionDrawable(new TextureRegion(baseTexture, 256, 406, 64, 50)), 
        		new TextureRegionDrawable(new TextureRegion(baseTexture, 320, 406, 64, 50)));
        btnSumbit.setPosition((AssetsGame._SreentWidth * 0.5f) + 64f, (AssetsGame._ScreentHeight - 192f) * 0.72f);
        btnSumbit.addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub              	
                super.touchUp(event, x, y, pointer, button);
            }
        	@Override
        	public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        		
        		String values = question.getValues(); //取值
        		String comparetype = question.getCompare(); //比较方式
        		String textValues = equalField.getText();
        		boolean isPass = false;
        		
        		if (comparetype.equals("=") ){
        			if(values.compareTo(textValues) == 0){
        				isPass = true;
        				btnQuestion.remove(); //不显示问题按钮
        				questionDialogWindow.remove();
        			}else{
        		        Image imageError = new Image(new TextureRegion(baseTexture, 768, 456, 128, 32 ));
        		        imageError.setPosition( (AssetsGame._SreentWidth - 384f) * 0.5f + 32f, (AssetsGame._ScreentHeight - 192f) * 0.72f );
        		        questionDialogWindow.addActor(imageError);
        			}
        		}
        		
        		//正确解答问题
        		if(isPass == true){
        			AssetsGame.history += (AssetsGame.history.length() > 0 ? ";" : "") + AssetsGame.currentSence; //必须在回答正确之后，才能进入下一关卡
        			AssetsGame.strMeno = AssetsGame.sceneItem.getHistoryContent() + ( AssetsGame.strMeno.length() > 0 ? ";" : "") + AssetsGame.strMeno;
        			AssetsGame.writeRecordData(); //保存游戏进度
        		}
        		        		
        		return true;
        	}
        });
        questionDialogWindow.addActor(btnSumbit);
        
        //取消
        ImageButton btnCancel = new ImageButton(new TextureRegionDrawable(new TextureRegion(baseTexture, 256, 456, 64, 50)), 
        		new TextureRegionDrawable(new TextureRegion(baseTexture, 320, 456, 64, 50)));
        btnCancel.setPosition((AssetsGame._SreentWidth * 0.5f) + 160f, (AssetsGame._ScreentHeight - 192f) * 0.72f);
        btnCancel.addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub              	
                super.touchUp(event, x, y, pointer, button);
            }
        	@Override
        	public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {            		
        		questionDialogWindow.remove();
        		return true;
        	}
        });
        questionDialogWindow.addActor(btnCancel);  
        
        stage.addActor(questionDialogWindow);        
	}	
	
	//填充人物图标
	public void doneCreatePeopleImage(Texture tx){
		
		peopleFrames[0] = new TextureRegion(tx, 0, 0, 128, 128); //王磊 wonglei
		peopleFrames[1] = new TextureRegion(tx, 128, 0, 128, 128); //李梓 lizi
		peopleFrames[2] = new TextureRegion(tx, 256, 0, 128, 128); //警察 police
		peopleFrames[3] = new TextureRegion(tx, 384, 0, 128, 128); //关小原 guangxianyuan
		peopleFrames[4] = new TextureRegion(tx, 512, 0, 128, 128); //阿诺 ano
		peopleFrames[5] = new TextureRegion(tx, 640, 0, 128, 128); //舍管阿姨 mather
		peopleFrames[6] = new TextureRegion(tx, 768, 0, 128, 128); //任是非 ren
		peopleFrames[7] = new TextureRegion(tx, 896, 0, 128, 128); //柳河东 liu
		peopleFrames[8] = new TextureRegion(tx, 0, 128, 128, 128); //福利院长 dean
		peopleFrames[9] = new TextureRegion(tx, 128, 128, 128, 128); //画廊店员 storer
		peopleFrames[10] = new TextureRegion(tx, 256, 128, 128, 128); //画廊老板 boss
		peopleFrames[11] = new TextureRegion(tx, 384, 128, 128, 128); //画外音提示形象 tiper_light
		peopleFrames[12] = new TextureRegion(tx, 384, 256, 128, 128); //画外音提示形象 tiper_harry（空白）
	}
	
	//人物图片索引 
	public TextureRegion doneGetPeopleImage(enumPeopleImage speakerCode){
		switch (speakerCode) {
			case wonglei:
				return peopleFrames[0];
			case lizi:
				return peopleFrames[1];
			case police:
				return peopleFrames[2];
			case guangxianyuan:
				return peopleFrames[3];
			case ano:
				return peopleFrames[4];
			case mather:
				return peopleFrames[5];
			case ren:
				return peopleFrames[6];
			case liu:
				return peopleFrames[7];
			case dean:
				return peopleFrames[8];
			case storer:
				return peopleFrames[9];
			case boss:
				return peopleFrames[10];
			case tiper_light:
				return peopleFrames[11];	
			case tiper_harry:
				return peopleFrames[12];
			default:
				return peopleFrames[11];
		}
	}
	
	//人物图片索引
	public TextureRegion doneGetPeopleImage(String speakerCode){

		if(speakerCode=="wonglei") 
			return peopleFrames[0];
		else if(speakerCode=="lizi") 
			return peopleFrames[1];
		else if(speakerCode=="police") 
			return peopleFrames[2];
		else if(speakerCode=="guangxianyuan") 
			return peopleFrames[3];
		else if(speakerCode=="ano") 
			return peopleFrames[4];
		else if(speakerCode=="mather") 
			return peopleFrames[5];
		else if(speakerCode=="ren") 
			return peopleFrames[6];
		else if(speakerCode=="liu") 
			return peopleFrames[7];
		else if(speakerCode=="dean") 
			return peopleFrames[8];
		else if(speakerCode=="storer") 
			return peopleFrames[9];
		else if(speakerCode=="boss") 
			return peopleFrames[10];
		else if(speakerCode=="tiper_light") 
			return peopleFrames[11];	
		else if(speakerCode=="tiper_harry") 
			return peopleFrames[12];
		else return peopleFrames[12];
	}	
	
	//场景背景图片索引
	public String doneGetSenceImageFilePath(int sencePtr){
		String sFilePath = "";
		switch (sencePtr) {
			case 1: //"Scene_Start_1":	
				sFilePath = "senceGif/Start.gif";
				break;
			case 2: //"Scene_Station_2":
				sFilePath = "senceGif/Station.gif";
				break;
			case 3: //"Scene_Restaurant_3":
				sFilePath = "senceGif/Restaurant.gif";
				break;
			case 4: //"Scene_Jianxing_4":
				sFilePath = "senceGif/Jianxing.gif";
				break;
			case 5: //"Scene_Assistant_Office_5":
				sFilePath = "senceGif/AssistantOffice.gif";
				break;
			case 6: //"Scene_Painter_Home_6":
				sFilePath = "senceGif/PainterHome.gif";
				break;
			case 7: //"Scene_Company_Dorm_7":
				sFilePath = "senceGif/CompanyDorm.gif";
				break;
			case 8: //"Scene_Techer_Dorm_8":
				sFilePath = "senceGif/TecherDorm.gif";
				break;
			case 9: //"Scene_Pear12_9":
				sFilePath = "senceGif/Pear12.gif";
				break;
			case 10: //"Scene_Children_Place_10":
				sFilePath = "senceGif/ChildrenPlace.gif";
				break;
			case 11: //"Scene_Painter_Home_11":
				sFilePath = "senceGif/PainterHome.gif";
				break;
			case 12: //"Scene_Village_12":
				sFilePath = "senceGif/Village.gif";
				break;
			case 13: //"Scene_Dorm_Place_13":
				sFilePath = "senceGif/DormPlace.gif";
				break;
			case 14: //"Scene_Police_Station_14":
				sFilePath = "senceGif/PoliceStation.gif";
				break;
			case 15: //"Scene_Company_Dorm_15":
				sFilePath = "senceGif/CompanyDorm.gif";
				break;
			case 16: //"Scene_Ren_Boos_Home_16":
				sFilePath = "senceGif/RenBoosHome.gif";
				break;
			case 17: //"Scene_Envelope_17":
				sFilePath = "senceGif/Envelope.gif";
				break;
			case 18: //"Scene_Computer_Room_18":
				sFilePath = "senceGif/ComputerRoom.gif";
				break;
			case 19: //"Scene_Government_Place_19":
				sFilePath = "senceGif/GovernmentPlace.gif";
				break;
			case 20: //"Scene_End_20":
				sFilePath = "senceGif/End.gif";
				break;
			default:
				break;
		}
		return sFilePath;		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		baseTexture.dispose();
		SenceTexture.dispose();
        stage.dispose(); 
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void render(float arg0) {
		// TODO Auto-generated method stub
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
        
        //填充文本内容、填充人物图标
        AssetsGame.script = AssetsGame.scriptList.get(AssetsGame.scriptIndex);
        String textMsg = AssetsGame.script.getContent();
        String talkerCode = AssetsGame.script.getSpeakerCode();
        batch.begin();
        batch.draw( doneGetPeopleImage(talkerCode), 
        		(AssetsGame._SreentWidth - 512f)*0.5f, AssetsGame._SreentSpaceY + 200f);
        scriptFont.drawWrapped(batch, textMsg, (AssetsGame._SreentWidth - 512f)*0.5f + 32, AssetsGame._SreentSpaceY + 180f, 448f );
        batch.end();
	}

	@Override
	public void resize(int arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean fling(float arg0, float arg1, int arg2) {
		// TODO Auto-generated method stub
		//由下向上
		if( ( arg0 > 0f ) && (arg1 < 0f ) ) {
			AssetsGame.scriptIndex++;
			if(AssetsGame.scriptIndex >= scriptCount){			
				AssetsGame.scriptIndex = 0;
			}
		}
		
		//由上向下
		if( ( arg0 < 0f ) && (arg1 > 0f ) ) {
			AssetsGame.scriptIndex--;
			if(AssetsGame.scriptIndex <= -1){			
				AssetsGame.scriptIndex = scriptCount - 1;
			}
		}
		
		return true;
	}

	@Override
	public boolean longPress(float arg0, float arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pan(float arg0, float arg1, float arg2, float arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pinch(Vector2 arg0, Vector2 arg1, Vector2 arg2, Vector2 arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean tap(float arg0, float arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(float arg0, float arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean zoom(float arg0, float arg1) {
		// TODO Auto-generated method stub
		return false;
	}

}
