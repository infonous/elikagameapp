package screent.game.ourcause;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import elika.game.ourcause.AssetsGame;
import elika.game.ourcause.MainActivity;

public class HelpScreent implements Screen, GestureListener {

	private Stage stage;	
	private Texture texture;
	
	private ImageButton btnMap;
	private ImageButton btnLibary;
	private ImageButton btnMeno;
	private ImageButton btnReturn;
	
	private int imageIndex = 0;
	private TextureRegion[]  textFrames = new TextureRegion[3];
	private SpriteBatch batch;
	
	public HelpScreent() {
		// TODO Auto-generated constructor stub
		super();
		if (stage == null) {
            stage = new Stage(AssetsGame._SreentWidth, AssetsGame._ScreentHeight, true);
            texture = new Texture(Gdx.files.internal("helpUI.png"));
            batch = new SpriteBatch();
            
            //填充背景图片
            Image imageBcakGroup = new Image(new TextureRegion(texture, 1016, 1016, 8, 8));
            imageBcakGroup.setFillParent(true);
            stage.addActor(imageBcakGroup);
            
            //填充背景图片：标题
            Image imageTitle = new Image(new TextureRegion(texture, 512, 712, 256, 50));
            imageTitle.setPosition(AssetsGame._SreentSpaceX, (AssetsGame._ScreentHeight - AssetsGame._SreentSpaceY - 50f));
            stage.addActor(imageTitle);
            
            //添加按钮，以及监听事件 
            doneCreateButtones();
            
            //切割主体图片
            textFrames[0] = new TextureRegion(texture, 0, 0, 512, 512); 
            textFrames[1] = new TextureRegion(texture, 512, 0, 512, 512);
            textFrames[2] = new TextureRegion(texture, 0, 512, 512, 512);
		}
	}

	//创建各按钮，以及对应的监听事件
	public void doneCreateButtones(){
		
        float btnX = AssetsGame._SreentWidth - AssetsGame._SreentSpaceX - 128f;
        float btnY = AssetsGame._SreentSpaceY;
        //按钮
        btnMap = new ImageButton(new TextureRegionDrawable(new TextureRegion(texture, 512, 512, 128, 50)), 
        		new TextureRegionDrawable(new TextureRegion(texture, 640, 512, 128, 50)));
        btnMap.setPosition(btnX, btnY + 150f);
        btnMap.addListener(new InputListener(){            	
            @Override
            public void touchUp(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub              	
                super.touchUp(event, x, y, pointer, button);
                MainActivity.game.setScreen(new MapPageScreent());
            }
            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub
                return true;
            }            
        });
        
        btnLibary = new ImageButton(new TextureRegionDrawable(new TextureRegion(texture, 512, 562, 128, 50)), 
        		new TextureRegionDrawable(new TextureRegion(texture, 640, 562, 128, 50)));
        btnLibary.setPosition(btnX, btnY + 100f);
        btnLibary.addListener(new InputListener(){            	
            @Override
            public void touchUp(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub              	
                super.touchUp(event, x, y, pointer, button);
                MainActivity.game.setScreen(new LibraryScreent());
            }
            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub
                return true;
            }                
        });
        
        btnMeno = new ImageButton(new TextureRegionDrawable(new TextureRegion(texture, 512, 612, 128, 50)), 
        		new TextureRegionDrawable(new TextureRegion(texture, 640, 612, 128, 50)));
        btnMeno.setPosition(btnX, btnY + 50f);
        btnMeno.addListener(new InputListener(){            	
            @Override
            public void touchUp(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub              	
                super.touchUp(event, x, y, pointer, button);
                MainActivity.game.setScreen(new MenoScreent());
            }
            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub
                return true;
            }                
        });
        
        btnReturn = new ImageButton(new TextureRegionDrawable(new TextureRegion(texture, 512, 662, 128, 50)), 
        		new TextureRegionDrawable(new TextureRegion(texture, 640, 662, 128, 50)));
        btnReturn.setPosition(btnX, btnY);
        btnReturn.addListener(new InputListener(){            	
            @Override
            public void touchUp(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub              	
                super.touchUp(event, x, y, pointer, button);
                MainActivity.game.setScreen(new MainPageScreent());
            }
            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub
                return true;
            }                
        });
        
        InputMultiplexer multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(stage);
        multiplexer.addProcessor(new GestureDetector(this));
        Gdx.input.setInputProcessor(multiplexer);
        
        stage.addActor(btnReturn);
        stage.addActor(btnMeno);
        stage.addActor(btnLibary);
        stage.addActor(btnMap); 
	}
	
	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		texture.dispose();
        stage.dispose(); 
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void render(float arg0) {
		// TODO Auto-generated method stub
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw(); 
        
        //填充内容
        batch.begin();
        batch.draw(textFrames[imageIndex], (AssetsGame._SreentWidth - 512f)*0.50f, (AssetsGame._ScreentHeight - 512f)*0.50f);
        
        batch.end();
	}

	@Override
	public void resize(int arg0, int arg1) {
		// TODO Auto-generated method stub
		AssetsGame.doneScreentChange();
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean fling(float arg0, float arg1, int arg2) {
		// TODO Auto-generated method stub
		//由下向上
		if( ( arg0 > 0f ) && (arg1 < 0f ) ) {
			imageIndex++;
			if(imageIndex > 2){			
				imageIndex = 0;
			}
		}
		
		//由上向下
		if( ( arg0 < 0f ) && (arg1 > 0f ) ) {
			imageIndex--;
			if(imageIndex < 0){			
				imageIndex = 2;
			}
		}
		
		return true;
	}

	@Override
	public boolean longPress(float arg0, float arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pan(float arg0, float arg1, float arg2, float arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pinch(Vector2 arg0, Vector2 arg1, Vector2 arg2, Vector2 arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean tap(float arg0, float arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(float arg0, float arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean zoom(float arg0, float arg1) {
		// TODO Auto-generated method stub
		return false;
	}

}
