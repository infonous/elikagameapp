package screent.game.ourcause;

import model.game.ourcause.MapInfo;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import elika.game.ourcause.AssetsGame;
import elika.game.ourcause.MainActivity;

public class MainPageScreent implements Screen {
	
	private Stage stage;	
	private Texture texture;
	private ImageButton btnMap;
	private ImageButton btnLibary;
	private ImageButton btnHistory;
	private ImageButton btnHelp;
	private ImageButton btnReplay;
	private ImageButton btnQuit;

	public MainPageScreent() {
		// TODO Auto-generated constructor stub
		super();
		if (stage == null) {
            stage = new Stage(AssetsGame._SreentWidth, AssetsGame._ScreentHeight, true);
            texture = new Texture(Gdx.files.internal("mainUI.png"));
            
            //填充背景
            Image imageBcakGroup = new Image(new TextureRegion(texture, 0, 660, 8, 8 ));
            imageBcakGroup.setFillParent(true);
            stage.addActor(imageBcakGroup);
            
            //填充背景图片：女孩戏水
            Image imageGril = new Image(new TextureRegion(texture, 0, 0, 1024, 670));
            imageGril.setPosition((AssetsGame._SreentWidth - 1024f)*0.5f, (AssetsGame._ScreentHeight - 670f)*0.5f);
            stage.addActor(imageGril);
            
            //填充背景图片：花 ；位置在右上角
            Image imageFlower = new Image(new TextureRegion(texture, 256, 720, 128, 64));
            imageFlower.setPosition(AssetsGame._SreentWidth - AssetsGame._SreentSpaceX - 64f, (AssetsGame._ScreentHeight - AssetsGame._SreentSpaceY - 50f));
            imageFlower.addAction(this.doneImageTitleAction());
            stage.addActor(imageFlower);
            
            //填充游戏标题图片
            Image imageTitle = new Image(new TextureRegion(texture, 256, 670, 256, 50));
            imageTitle.setPosition(AssetsGame._SreentSpaceX, (AssetsGame._ScreentHeight - AssetsGame._SreentSpaceY - 50f));
            stage.addActor(imageTitle);
            
            //填充出版信息
            Image imageCopyright = new Image(new TextureRegion(texture, 0, 992, 512, 32));
            imageCopyright.setPosition((AssetsGame._SreentWidth - 512f)*0.5f, AssetsGame._SreentSpaceY);
            stage.addActor(imageCopyright);    
            
            //加入各按钮
            doneCreateButtones();
           
		}
	}
	
	//加入各按钮
	public void doneCreateButtones(){
		
        float btnX = AssetsGame._SreentWidth - AssetsGame._SreentSpaceX - 128f;
        float btnY = (AssetsGame._ScreentHeight - 300f)*0.5f;
        //按钮：地图、 图书馆、备忘本、游戏帮助、新再玩、退出
        btnMap = new ImageButton(new TextureRegionDrawable(new TextureRegion(texture, 0, 670, 128, 50)), 
        		new TextureRegionDrawable(new TextureRegion(texture, 128, 670, 128, 50)));
        btnMap.setPosition(btnX, btnY + 250f);
        btnMap.addListener(new InputListener(){            	
            @Override
            public void touchUp(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub              	
                super.touchUp(event, x, y, pointer, button);
                MainActivity.game.setScreen(new MapPageScreent());
            }
            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub
                return true;
            }                
        });
        
        btnLibary = new ImageButton(new TextureRegionDrawable(new TextureRegion(texture, 0, 720, 128, 50)), 
        		new TextureRegionDrawable(new TextureRegion(texture, 128, 720, 128, 50)));
        btnLibary.setPosition(btnX, btnY + 200f);
        btnLibary.addListener(new InputListener(){            	
            @Override
            public void touchUp(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub              	
                super.touchUp(event, x, y, pointer, button);
                MainActivity.game.setScreen(new LibraryScreent());
            }
            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub
                return true;
            }                
        });
        
        btnHistory = new ImageButton(new TextureRegionDrawable(new TextureRegion(texture, 0, 770, 128, 50)), 
        		new TextureRegionDrawable(new TextureRegion(texture, 128, 770, 128, 50)));
        btnHistory.setPosition(btnX, btnY + 150f);
        btnHistory.addListener(new InputListener(){            	
            @Override
            public void touchUp(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub              	
                super.touchUp(event, x, y, pointer, button);
                MainActivity.game.setScreen(new MenoScreent());
            } 
            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub
                return true;
            } 
        });            
        
        btnHelp = new ImageButton(new TextureRegionDrawable(new TextureRegion(texture, 0, 820, 128, 50)), 
        		new TextureRegionDrawable(new TextureRegion(texture, 128, 820, 128, 50)));
        btnHelp.setPosition(btnX, btnY + 100f);
        btnHelp.addListener(new InputListener(){            	
            @Override
            public void touchUp(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub              	
                super.touchUp(event, x, y, pointer, button);
                MainActivity.game.setScreen(new HelpScreent());
            }
            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub
                return true;
            }                
        });
        
        btnReplay = new ImageButton(new TextureRegionDrawable(new TextureRegion(texture, 0, 870, 128, 50)), 
        		new TextureRegionDrawable(new TextureRegion(texture, 128, 870, 128, 50)));
        btnReplay.setPosition(btnX, btnY + 50f);
        btnReplay.addListener(new InputListener(){            	
            @Override
            public void touchUp(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub              	
                super.touchUp(event, x, y, pointer, button);
                
                AssetsGame.clearReordData();
                MainActivity.game.setScreen(new SenceScreent(new MapInfo("Start", "序幕"), null));
            }
            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub
                return true;
            }                
        });
        
        
        btnQuit = new ImageButton(new TextureRegionDrawable(new TextureRegion(texture, 0, 920, 128, 50)), 
        		new TextureRegionDrawable(new TextureRegion(texture, 128, 920, 128, 50)));
        btnQuit.setPosition(btnX, btnY);
        btnQuit.addListener(new InputListener(){            	
            @Override
            public void touchUp(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub              	
                super.touchUp(event, x, y, pointer, button);
                System.exit(0);
                android.os.Process.killProcess(android.os.Process.myPid());
            }
            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                    int pointer, int button) {
                // TODO Auto-generated method stub
                return true;
            }                
        });
        
        Gdx.input.setInputProcessor(stage);
        stage.addActor(btnQuit);
        stage.addActor(btnReplay);
        stage.addActor(btnHelp);
        stage.addActor(btnHistory);
        stage.addActor(btnLibary);
        stage.addActor(btnMap); 
	}
	
	//Scale 比例  duration 持续时间 
   public RepeatAction doneImageTitleAction() {	   
	   //效果：按路线移动 ; 花 ；位置在左上角
	   MoveToAction moveto1 = Actions.moveTo(AssetsGame._SreentWidth*0.8f, AssetsGame._ScreentHeight*0.2f, 12f);
	   MoveToAction moveto2 = Actions.moveTo(AssetsGame._SreentWidth*0.4f, AssetsGame._ScreentHeight*0.6f, 6f);
	   MoveToAction moveto3 = Actions.moveTo(AssetsGame._SreentWidth*0.5f, AssetsGame._ScreentHeight*0.5f, 6f);
	   MoveToAction moveto4 = Actions.moveTo(AssetsGame._SreentWidth*0.2f, AssetsGame._ScreentHeight*0.1f, 6f);

       SequenceAction moveto = Actions.sequence(Actions.fadeIn(4f), moveto1, moveto2, moveto3, Actions.fadeIn(2f), Actions.fadeOut(4f), Actions.fadeIn(4f), moveto4);
       RepeatAction repeat = Actions.repeat(3, moveto);
       
       return repeat;
   }

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		texture.dispose();
        stage.dispose(); 
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void render(float arg0) {
		// TODO Auto-generated method stub
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw(); 
	}

	@Override
	public void resize(int arg0, int arg1) {
		// TODO Auto-generated method stub
		AssetsGame.doneScreentChange();
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

}
