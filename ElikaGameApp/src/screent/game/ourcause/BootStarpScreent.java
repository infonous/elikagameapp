package screent.game.ourcause;

import model.game.ourcause.MapInfo;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import elika.game.ourcause.AssetsGame;
import elika.game.ourcause.MainActivity;

public class BootStarpScreent implements Screen {

	private Stage stage;	
	private Texture texture;
	
	public BootStarpScreent() {
		// TODO Auto-generated constructor stub
		super();
		if (stage == null) {
            stage = new Stage(AssetsGame._SreentWidth, AssetsGame._ScreentHeight, true);
            texture = new Texture(Gdx.files.internal("bootStarpUI.png"));
            
            //填充背景图片
            Image imageBcakGroup = new Image(new TextureRegion(texture, 0, 472, 8, 8));
            imageBcakGroup.setFillParent(true);
            stage.addActor(imageBcakGroup);
            
            //填充背景图片：落花流水
            Image imageFlower = new Image(new TextureRegion(texture, 0, 0, 448, 472));
            imageFlower.setPosition((AssetsGame._SreentWidth - 512f)*0.5f, (AssetsGame._ScreentHeight - 472f)*0.5f);
            stage.addActor(imageFlower);
            
            //填充背景图片：标题动画
            Image imageTitle = new Image(new TextureRegion(texture, 448, 0, 64, 320));
            imageTitle.setPosition((AssetsGame._SreentWidth*0.5f) + 96f, (AssetsGame._ScreentHeight - 320f)*0.5f);
            imageTitle.addAction(this.doneImageTitleAction());
            stage.addActor(imageTitle);
            
            //出版信息
            Image imageCopyright = new Image(new TextureRegion(texture, 0, 480, 512, 32));
            imageCopyright.setPosition((AssetsGame._SreentWidth - 512f)*0.5f, AssetsGame._SreentSpaceY);
            stage.addActor(imageCopyright);
		}
	}
	
	//Scale 比例  duration 持续时间 
   public SequenceAction doneImageTitleAction() {      
	   
	   //效果： 淡入淡出 3秒
	   float duration = 3f;
       Action stopAction = Actions.run(new Runnable() {
           @Override
           public void run() {
        	   
	        	if(AssetsGame.history == ""){	        		
	        		MainActivity.game.setScreen(new SenceScreent(new MapInfo("Start", "序幕"), null));
	        	}else{
	        		
	        		MainActivity.game.setScreen(new MainPageScreent());
	        	}
           }
       });

       SequenceAction alpha = Actions.sequence(Actions.fadeIn(duration), Actions.fadeOut(duration), stopAction );
       
       return alpha;
   }
	   

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		texture.dispose();
        stage.dispose(); 
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void render(float arg0) {
		// TODO Auto-generated method stub
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw(); 
	}

	@Override
	public void resize(int arg0, int arg1) {
		// TODO Auto-generated method stub
		AssetsGame.doneScreentChange();
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

}
