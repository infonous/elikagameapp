package screent.game.ourcause;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import elika.game.ourcause.AssetsGame;
import elika.game.ourcause.MainActivity;

public class LibraryScreent implements Screen, GestureListener  {
	
	private Stage stage;	
	private Texture texture;
	private ImageButton btnMap;
	private ImageButton btnHistory;
	private ImageButton btnHelp;
	private ImageButton btnReturn;	

	private int imageIndex = 0;
	private TextureRegion[]  textFrames = new TextureRegion[8];
	private TextureRegion TexturePointBlur;
	private TextureRegion TexturePointFocus;
	
	private SpriteBatch batch;

	public LibraryScreent() {
		// TODO Auto-generated constructor stub
		super();
		if (stage == null) {
            stage = new Stage(AssetsGame._SreentWidth, AssetsGame._ScreentHeight, true);
            texture = new Texture(Gdx.files.internal("libaryUI.png"));            
            batch = new SpriteBatch();
            
            //填充背景
            Image imageBcakGroup = new Image(new TextureRegion(texture, 1016, 64, 8, 8 ));
            imageBcakGroup.setFillParent(true);
            stage.addActor(imageBcakGroup);
            
            //填充游戏标题
            Image imageTitle = new Image(new TextureRegion(texture, 0, 960, 320, 50));
            imageTitle.setPosition(AssetsGame._SreentSpaceX, (AssetsGame._ScreentHeight - AssetsGame._SreentSpaceY - 50f));
            stage.addActor(imageTitle);  
            
            //填充点缀：左下角
            Image imageFun = new Image(new TextureRegion(texture, 645, 840, 320, 120)); //640
            imageFun.setPosition(AssetsGame._SreentSpaceX, AssetsGame._SreentSpaceY);
            stage.addActor(imageFun);
            
            //填充各个文本内容
            textFrames[0] = new TextureRegion(texture, 0, 0, 320, 320);
            textFrames[1] = new TextureRegion(texture, 320, 0, 320, 320);
            textFrames[2] = new TextureRegion(texture, 640, 0, 320, 320);
            textFrames[3] = new TextureRegion(texture, 0, 320, 320, 320);
            textFrames[4] = new TextureRegion(texture, 320, 320, 320, 320);
            textFrames[5] = new TextureRegion(texture, 640, 320, 320, 320);
            textFrames[6] = new TextureRegion(texture, 0, 640, 320, 320);
            textFrames[7] = new TextureRegion(texture, 320, 640, 320, 320);            
            
            //填充页面加点         
            TexturePointBlur = new TextureRegion(texture, 992, 0, 32, 32);
            TexturePointFocus= new TextureRegion(texture, 992, 32, 32, 32);            
                        
            float btnX = AssetsGame._SreentWidth - AssetsGame._SreentSpaceX - 128f;
            float btnY = AssetsGame._ScreentHeight * 0.35f;
            //按钮：地图、 图书馆、备忘本、游戏帮助、新再玩、退出
            btnMap = new ImageButton(new TextureRegionDrawable(new TextureRegion(texture, 645, 640, 128, 50)), 
            		new TextureRegionDrawable(new TextureRegion(texture, 773, 640, 128, 50)));
            btnMap.setPosition(btnX, btnY + 150f);
            btnMap.addListener(new InputListener(){            	
                @Override
                public void touchUp(InputEvent event, float x, float y,
                        int pointer, int button) {
                    // TODO Auto-generated method stub              	
                    super.touchUp(event, x, y, pointer, button);
                    MainActivity.game.setScreen(new MapPageScreent());
                }
                @Override
                public boolean touchDown(InputEvent event, float x, float y,
                        int pointer, int button) {
                    // TODO Auto-generated method stub
                    return true;
                }                
            });
            
            btnHistory = new ImageButton(new TextureRegionDrawable(new TextureRegion(texture, 645, 690, 128, 50)), 
            		new TextureRegionDrawable(new TextureRegion(texture, 773, 690, 128, 50)));
            btnHistory.setPosition(btnX, btnY + 100f);
            btnHistory.addListener(new InputListener(){            	
                @Override
                public void touchUp(InputEvent event, float x, float y,
                        int pointer, int button) {
                    // TODO Auto-generated method stub              	
                    super.touchUp(event, x, y, pointer, button);
                    MainActivity.game.setScreen(new MenoScreent());
                } 
                @Override
                public boolean touchDown(InputEvent event, float x, float y,
                        int pointer, int button) {
                    // TODO Auto-generated method stub
                    return true;
                } 
            });

            btnHelp = new ImageButton(new TextureRegionDrawable(new TextureRegion(texture, 645, 740, 128, 50)), 
            		new TextureRegionDrawable(new TextureRegion(texture, 773, 740, 128, 50)));
            btnHelp.setPosition(btnX, btnY + 50f);
            btnHelp.addListener(new InputListener(){            	
                @Override
                public void touchUp(InputEvent event, float x, float y,
                        int pointer, int button) {
                    // TODO Auto-generated method stub              	
                    super.touchUp(event, x, y, pointer, button);
                    MainActivity.game.setScreen(new HelpScreent());
                }
                @Override
                public boolean touchDown(InputEvent event, float x, float y,
                        int pointer, int button) {
                    // TODO Auto-generated method stub
                    return true;
                }                
            });
            
            btnReturn = new ImageButton(new TextureRegionDrawable(new TextureRegion(texture, 645, 790, 128, 50)), 
            		new TextureRegionDrawable(new TextureRegion(texture, 773, 790, 128, 50)));
            btnReturn.setPosition(btnX, btnY);
            btnReturn.addListener(new InputListener(){            	
                @Override
                public void touchUp(InputEvent event, float x, float y,
                        int pointer, int button) {
                    // TODO Auto-generated method stub              	
                    super.touchUp(event, x, y, pointer, button);
                    MainActivity.game.setScreen(new MainPageScreent());
                }
                @Override
                public boolean touchDown(InputEvent event, float x, float y,
                        int pointer, int button) {
                    // TODO Auto-generated method stub
                    return true;
                }                
            });
            
            InputMultiplexer multiplexer = new InputMultiplexer();
            multiplexer.addProcessor(stage);
            multiplexer.addProcessor(new GestureDetector(this));
            Gdx.input.setInputProcessor(multiplexer);
 
            stage.addActor(btnReturn);
            stage.addActor(btnHelp);
            stage.addActor(btnHistory);
            stage.addActor(btnMap);            
		}
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		texture.dispose();
        stage.dispose(); 
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void render(float arg0) {
		// TODO Auto-generated method stub
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw(); 
        
        //填充文本内容
        batch.begin();
        batch.draw(textFrames[imageIndex], (AssetsGame._SreentWidth - 320f)*0.50f, (AssetsGame._ScreentHeight - 320f)*0.65f);
	
        //当前位置的标识显示: 覆盖性显示，呵呵，这算法ZZ
        for (int i=0; i < 8; i++){
			batch.draw(((imageIndex==i)?TexturePointFocus:TexturePointBlur), ( (AssetsGame._SreentWidth - 400f)*0.5f + (i*50f) ), ( (AssetsGame._ScreentHeight - 320f)*0.65f - 180f) );
		}
        
        batch.end();
	}

	@Override
	public void resize(int arg0, int arg1) {
		// TODO Auto-generated method stub
		AssetsGame.doneScreentChange();
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	/*
	 fling(float velocityX, float velocityY, int button) ：翻页动作。
	     第一个参数：X 轴方向滑动的速度。
	     第二个参数：Y 轴方向滑动的速度。
	    第三个参数是：鼠标按键的编码，鼠标左键是 0 ，鼠标右键是 1 。
    */
	@Override
	public boolean fling(float arg0, float arg1, int arg2) {
		// TODO Auto-generated method stub
		
		//由下向上
		if( ( arg0 > 0f ) && (arg1 < 0f ) ) {
			imageIndex++;
			if(imageIndex > 7){			
				imageIndex = 0;
			}
		}
		
		//由上向下
		if( ( arg0 < 0f ) && (arg1 > 0f ) ) {
			imageIndex--;
			if(imageIndex < 0){			
				imageIndex = 7;
			}
		}
		
		return true;
	}

	@Override
	public boolean longPress(float arg0, float arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pan(float arg0, float arg1, float arg2, float arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pinch(Vector2 arg0, Vector2 arg1, Vector2 arg2, Vector2 arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean tap(float arg0, float arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(float arg0, float arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean zoom(float arg0, float arg1) {
		// TODO Auto-generated method stub
		return false;
	}

}
