package screent.game.ourcause;

import actor.game.ourcause.MapBuilderBtnActor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import elika.game.ourcause.AssetsGame;
import elika.game.ourcause.MainActivity;

public class MapPageScreent implements Screen {
	
	private Stage stage;	
	private Texture texture;
	private ImageButton btnLibary;
	private ImageButton btnHistory;
	private ImageButton btnHelp;
	private ImageButton btnReturn;

	public MapPageScreent() {
		// TODO Auto-generated constructor stub
		super();
		if (stage == null) {
            stage = new Stage(AssetsGame._SreentWidth, AssetsGame._ScreentHeight, true);
            texture = new Texture(Gdx.files.internal("mapUI.png"));
            
            //填充背景
            Image imageBcakGroup = new Image(new TextureRegion(texture, 0, 0, 8, 8));
            imageBcakGroup.setFillParent(true);
            stage.addActor(imageBcakGroup);
            
            //填充地图背景图片
            Image imageGril = new Image(new TextureRegion(texture, 0, 0, 1024, 512));
            imageGril.setPosition((AssetsGame._SreentWidth - 1024f)*0.5f, (AssetsGame._ScreentHeight - 512f)*0.5f);
            stage.addActor(imageGril);            
            
            //按钮：图书馆、备忘本、游戏帮助、返回首页
            float btnX = AssetsGame._SreentWidth - AssetsGame._SreentSpaceX - 128f;
            float btnY = AssetsGame._SreentSpaceY;
            btnLibary = new ImageButton(new TextureRegionDrawable(new TextureRegion(texture, 0, 512, 128, 50)), 
            		new TextureRegionDrawable(new TextureRegion(texture, 128, 512, 128, 50)));
            btnLibary.setPosition(btnX, btnY + 150f);
            btnLibary.addListener(new InputListener(){            	
                @Override
                public void touchUp(InputEvent event, float x, float y,
                        int pointer, int button) {
                    // TODO Auto-generated method stub              	
                    super.touchUp(event, x, y, pointer, button);
                    MainActivity.game.setScreen(new LibraryScreent());
                }  
                @Override
                public boolean touchDown(InputEvent event, float x, float y,
                        int pointer, int button) {
                    // TODO Auto-generated method stub
                    return true;
                } 
            });
            
            btnHistory = new ImageButton(new TextureRegionDrawable(new TextureRegion(texture, 0, 562, 128, 50)), 
            		new TextureRegionDrawable(new TextureRegion(texture, 128, 562, 128, 50)));
            btnHistory.setPosition(btnX, btnY + 100f);
            btnHistory.addListener(new InputListener(){            	
                @Override
                public void touchUp(InputEvent event, float x, float y,
                        int pointer, int button) {
                    // TODO Auto-generated method stub              	
                    super.touchUp(event, x, y, pointer, button);
                    MainActivity.game.setScreen(new MenoScreent());
                } 
                @Override
                public boolean touchDown(InputEvent event, float x, float y,
                        int pointer, int button) {
                    // TODO Auto-generated method stub
                    return true;
                } 
            });             
            
            btnHelp = new ImageButton(new TextureRegionDrawable(new TextureRegion(texture, 0, 612, 128, 50)), 
            		new TextureRegionDrawable(new TextureRegion(texture, 128, 612, 128, 50)));
            btnHelp.setPosition(btnX, btnY + 50f);
            btnHelp.addListener(new InputListener(){            	
                @Override
                public void touchUp(InputEvent event, float x, float y,
                        int pointer, int button) {
                    // TODO Auto-generated method stub              	
                    super.touchUp(event, x, y, pointer, button);
                    MainActivity.game.setScreen(new HelpScreent());
                }
                @Override
                public boolean touchDown(InputEvent event, float x, float y,
                        int pointer, int button) {
                    // TODO Auto-generated method stub
                    return true;
                }                
            });
            
            btnReturn = new ImageButton(new TextureRegionDrawable(new TextureRegion(texture, 0, 662, 128, 50)), 
            		new TextureRegionDrawable(new TextureRegion(texture, 128, 662, 128, 50)));
            btnReturn.setPosition(btnX, btnY);
            btnReturn.addListener(new InputListener(){            	
                @Override
                public void touchUp(InputEvent event, float x, float y,
                        int pointer, int button) {
                    // TODO Auto-generated method stub              	
                    super.touchUp(event, x, y, pointer, button);
                    MainActivity.game.setScreen(new MainPageScreent());
                }
                @Override
                public boolean touchDown(InputEvent event, float x, float y,
                        int pointer, int button) {
                    // TODO Auto-generated method stub
                    return true;
                }                
            });
            
            //填充建筑地点 
            MapBuilderBtnActor builder = new MapBuilderBtnActor(new TextureRegion(texture, 0, 712, 960, 128));
            
            //监听事件
            Gdx.input.setInputProcessor(stage);
            stage.addActor(btnReturn);
            stage.addActor(btnHelp);
            stage.addActor(btnHistory);
            stage.addActor(btnLibary);
            stage.addActor(builder);
		}
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		texture.dispose();
        stage.dispose(); 
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void render(float arg0) {
		// TODO Auto-generated method stub
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw(); 
	}

	@Override
	public void resize(int arg0, int arg1) {
		// TODO Auto-generated method stub
		AssetsGame.doneScreentChange();
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

}
