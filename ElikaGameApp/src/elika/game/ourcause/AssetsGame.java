package elika.game.ourcause;

import java.util.List;

import model.game.ourcause.Scene;
import model.game.ourcause.SceneEntity;
import model.game.ourcause.Script;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

public class AssetsGame {
	
	//实际的屏幕长宽和基本的长宽参数
	public static float _SreentWidth; //屏幕宽度
	public static float _ScreentHeight; //屏幕高度
	public static float _SreentSpaceX; //屏幕边框的宽度: X 轴方向
	public static float _SreentSpaceY; //屏幕边框的宽度: Y 轴方向
	
	//场景和脚本的基本参数	
	public static int scriptIndex = 0; //脚本默认索引
    public static String history = ""; //所有已访问过的场景代码列表字串
    public static String strMeno = ""; //所有已访问过的场景备注字串
    public static String currentSence = ""; //当前场景代码
    public static List<Scene> sceneStore = null; //所有的场景 list
    public static Scene sceneItem;	//保存当前场景
    public static List<Script> scriptList = null; //保存当前剧本（正在访问或已经访问过的剧本）
    public static Script script = null; //当前脚本索引内容
    

	public AssetsGame() {
		// TODO Auto-generated constructor stub
	}
	
	public static void load(){
		//屏幕的基本长宽和边框宽度设置
		AssetsGame._SreentWidth = Gdx.graphics.getWidth();
		AssetsGame._ScreentHeight = Gdx.graphics.getHeight();		
		AssetsGame._SreentSpaceX = AssetsGame._SreentWidth*0.05f;
		AssetsGame._SreentSpaceY = AssetsGame._ScreentHeight*0.05f;
		
		readRecordData(); //读取游戏历史进度记录: history ^ strMeno 格式保存
		
		SceneEntity entity = new SceneEntity();
		AssetsGame.sceneStore = entity.initSceneStore();
	}
	
	public static void doneScreentChange(){		
		AssetsGame._SreentWidth = Gdx.graphics.getWidth();
		AssetsGame._ScreentHeight = Gdx.graphics.getHeight();
	}
	
	//读取游戏记录
	public static void readRecordData(){

		FileHandle file = Gdx.files.local("sugarRecord.txt");
		if(file.exists()){

			String strRecord = file.readString();			
			if((strRecord.indexOf("~") > -1)){
				
				String strSplit[] = strRecord.split("~"); //history ~ strMeno
				AssetsGame.history = strSplit[0];
				AssetsGame.strMeno = strSplit[1];
			}	
		}else{
			file.writeString("", false);
		}
	}
	
	//覆盖性写入游戏记录
	public static void writeRecordData(){
		
		FileHandle file = Gdx.files.local("sugarRecord.txt");
		file.writeString(AssetsGame.history + "~" + AssetsGame.strMeno, false); 
	}
	
	//清除：重新玩一次
	public static void clearReordData(){
		
		FileHandle file = Gdx.files.local("sugarRecord.txt");
		file.writeString("", false);
		
		AssetsGame.history = "";
		AssetsGame.strMeno = "";
	}
}
