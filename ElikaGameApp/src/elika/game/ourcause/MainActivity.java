package elika.game.ourcause;

import screent.game.ourcause.BootStarpScreent;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

import android.os.Bundle;

public class MainActivity extends AndroidApplication {
	public static Game game = null;
    
	@Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		config.useGL20 = false;		
		game = new Game() {

			@Override
			public void create() {
				// TODO Auto-generated method stub
				AssetsGame.load();
				game.setScreen(new BootStarpScreent());
			}
		};
		
		initialize(game, config);
    }

}